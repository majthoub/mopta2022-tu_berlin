import sys
sys.path = sorted(sys.path, key=lambda x: ('venv' in x), reverse=True)
import dash
from dash import dcc, html, callback_context
from dash import Input, Output, State
from dash.exceptions import PreventUpdate

import policies as pol
import block_scheduling as bs
import assignments as ass
import pandas as pd
import plotly.express as px
import numpy as np

#########################################################3
# Custom encoder to handle tuples
#########################################################3
def key_converter(key):
    if isinstance(key,tuple):
        return '__TUPLE__'+str(key)
    else:
        return key

def tuple_converter(obj):
    if isinstance(obj, list):
        return [tuple_converter(e) for e in obj]
    elif isinstance(obj, tuple):
        return {'__tuple__': True, 'items': [tuple_converter(e) for e in obj]}
    elif isinstance(obj, dict):
        return {key_converter(key): tuple_converter(value) for key, value in obj.items()}
    else:
        return obj

def key_tuple_reader(key):
    if isinstance(key,str) and key.startswith('__TUPLE__'):
        return eval(key[9:])
    else:
        return key

def hinted_tuple_reader(obj):
    if isinstance(obj, list):
        return [hinted_tuple_reader(e) for e in obj]
    elif isinstance(obj, dict):
        if '__tuple__' in obj:
            return tuple([hinted_tuple_reader(e) for e in obj['items']])
        else:
            return {key_tuple_reader(key): hinted_tuple_reader(value) for key, value in obj.items()}
    else:
        return obj
#########################################################3


STRICT_START_DETERMINISTIC_ASG = False
MIP_TIME_LIMIT = 30.

app = dash.Dash()
app.layout = html.Div([
    html.H1(children='Instance parameters'),
    html.Label(children='Instance seed'), dcc.Input(id="seed", type='number', value=42),
    html.Br(),
    html.Label(children='Number of patients'), dcc.Dropdown([70, 100, 140, 200], 100, id='patient-number'),
    html.Br(),
    html.Label(children='Second-stage costs'), dcc.Dropdown(['co=1, cw=0.5, ci=0.25',
                                                             'co=1, cw=0.5, ci=0.',
                                                             'co=1, cw=0., ci=0.'], 'co=1, cw=0.5, ci=0.25', id='second-stage-costs'),
    html.Br(),
    html.Label(children='Emergency Rate'), dcc.Dropdown([0, 1, 2, 3], 1, id='emergency-rate'),
    html.Br(),
    html.Label(children='Flowtime unit'), dcc.Dropdown(['day', 'week'], 'day', id='flowunit'),
    html.Br(),
    html.Label(children='Scheduling cost Weight'), dcc.Input(id='w0', type="number", value=0.43),
    dcc.Store(id='instance_data'),
    dcc.Store(id='method_data'),
    html.Br(),
    html.Label(children='Optimization Method'), dcc.Dropdown(['Deterministic', '2-stage Stochastic', 'First-Fit'], id="method", value='Deterministic'),
    html.Div(children=[
        html.Label('Job duration ', style={'display': 'block'}, id='deterministic-duration-label'),
        dcc.Dropdown(['mean',
                      '60th quantile',
                      '70th quantile',
                      '80th quantile',
                      '90th quantile',
                       ],
                     id='deterministic-duration',
                     value='70th quantile',
                     style={'display': 'block', 'width': '15vw'}),
        html.Label('Capacity ', style={'display': 'block'}, id='capacity-label'),
        dcc.Input(id='capacity',value=500, style={'display': 'block', 'width': '5vw'}, type="number"),
        html.Label('Max considered emergencies ', style={'display': 'block'}, id='max-emer-label'),
        dcc.Dropdown([0,1,2,3,4,5,6,7,8,9,10],id='max-emer',value=4,
                     style={'display': 'block', 'width': '10vw'}),
        html.Label('Number scenarios for setting tentative starts ', style={'display': 'block'},
                   id='scenarios-tentative-label'),
        dcc.Dropdown([100,250,500,1000,3000],id='scenarios-tentative',value=250,
                     style={'display': 'block', 'width': '5vw'})

    ], style={'display': 'flex', 'flex-direction': 'row'}, id='method-params'),
    html.Br(),
    html.Button('Optimize Assignment', id='optimize', style={'margin-left': 10}),
    html.Br(),
    html.Label(children='Scenario used for visualization {1,2,...}: '), dcc.Input(id="scenario", type='number', value=1),
    dcc.Store(id='assignment_data'),
    html.Br(),
    html.H1(children='Policy parameters'),
    html.Br(),
    html.Label(children='Alpha-param (start emergency if 100·α% fits in idle time) '), dcc.Input(id="alpha", type='number', value=0.8),
    html.Br(),
    html.Label(children='δ-param (Cancel job if starts after T+δ) '), dcc.Input(id="delta", type='number', value=1000),
    html.Br(),
    html.Label(children='Δ-param (Cancel last job if exp. load ≥ T+Δ) '), dcc.Input(id="delta2", type='number', value=1000),
    html.Br(),
    html.Label(children='current time'),
    dcc.Slider(0, 5,
               step=0.01,
               marks={
                    0: 'Mon',
                    1: 'Tue',
                    2: 'Wed',
                    3: 'Thu',
                    4: 'Fri',
                    5: 'End'
               },
               value=5,
               id = 'current-time'
               ),
    html.Div([], id='graph-container'),
    html.H1(children='Monte-Carlo Simulation'),
    html.Label(children='Number scenario simulations'), dcc.Input(id="scenario-n", type='number', value=200),
    html.Button('Simulate Cost Distribution', id='simulation', style={'margin-left': 10}),
    html.Div([], id='simulation-container'),
    ],
    style={'padding': 10, 'flex': 1})


@app.callback(
    Output(component_id='instance_data', component_property='data'),
    Input(component_id='seed', component_property='value'),
    Input(component_id='patient-number', component_property='value'),
    Input(component_id='second-stage-costs', component_property='value'),
    Input(component_id='emergency-rate', component_property='value'),
    Input(component_id='flowunit', component_property='value'),
    Input(component_id='w0', component_property='value'),
)
def load_instance(seed, n, ssc, rate,unit,w0):

    co, cw, ci = {'co=1, cw=0.5, ci=0.25': (1,0.5,0.25),
                'co=1, cw=0.5, ci=0.':   (1,0.5,0.),
                'co=1, cw=0., ci=0.': (1, 0., 0.)}[ssc]

    instance_data = {'n': n,
                     'seed': seed,
                     'rate': rate,
                     'co': co,
                     'ci': ci,
                     'cw': cw,
                     'w0': w0,
                     'flowunit': unit
                     }

    return instance_data

@app.callback(
    Output(component_id='w0', component_property='value'),
    Input(component_id='flowunit', component_property='value')
)
def update_default_w0(unit):
    if unit=='week':
        return 19.
    else:
        return 0.43


@app.callback(
    Output(component_id='deterministic-duration', component_property='style'),
    Output(component_id='max-emer', component_property='style'),
    Output(component_id='scenarios-tentative', component_property='style'),
    Output(component_id='capacity', component_property='style'),
    Output(component_id='deterministic-duration-label', component_property='style'),
    Output(component_id='max-emer-label', component_property='style'),
    Output(component_id='scenarios-tentative-label', component_property='style'),
    Output(component_id='capacity-label', component_property='style'),
    Input(component_id='method', component_property='value')
)
def update_method_params(method):
    hide = {'display': 'none'}
    show = {'display': 'block'}
    duration_display = {'display': 'block', 'width': '15vw'}
    maxemer_display = {'display': 'block', 'width': '10vw'}
    if method == 'Deterministic':
        return duration_display, hide, hide, hide, show, hide, hide, hide
    elif method=='First-Fit':
        return duration_display, hide, hide, maxemer_display, show, hide, hide, show
    else:
        return hide, maxemer_display,maxemer_display,hide, hide,show,show, hide

@app.callback(
    Output(component_id='method_data', component_property='data'),
    Input(component_id='deterministic-duration', component_property='value'),
    Input(component_id='max-emer', component_property='value'),
    Input(component_id='scenarios-tentative', component_property='value'),
    Input(component_id='method', component_property='value'),
    Input(component_id='capacity', component_property='value'),
)
def update_method_params(det_duration, max_emer, scenarios_tentative, method, capacity):
    method_data = {
        'deterministic_duration': det_duration,
        'max_emergencies': max_emer,
        'scenarios_tentative': scenarios_tentative,
        'method': method,
        'capacity': capacity
    }
    return method_data


def get_instance_crit(instance_data):
    instance = bs.BlockSchedulingInstance.from_seed(instance_data['n'],
                                                    instance_data['rate'],
                                                    instance_data['seed'])
    assert instance_data['co'] == 1
    crit = bs.Criterion(instance, waiting_cost=instance_data['cw'], idling_cost=instance_data['ci'],
                        w0=instance_data['w0'], flowtime_unit=instance_data['flowunit'])
    return instance, crit

def get_assignment(instance, criterion, assignment_data):
    asg_data = hinted_tuple_reader(assignment_data)
    asg = asg_data['assignment']
    starts = asg_data['starts']
    return ass.Assignment(instance, criterion, assignment=asg, starts=starts)

@app.callback(
    Output(component_id='assignment_data', component_property='data'),
    Input(component_id='optimize', component_property='n_clicks'),
    State(component_id='instance_data', component_property='data'),
    State(component_id='method_data', component_property='data'),
)
def optimize(button, instance_data, method_data):
    ctx = callback_context
    if not ctx.triggered:
        raise PreventUpdate
    if button is None or instance_data is None:
        raise PreventUpdate

    instance, crit = get_instance_crit(instance_data)
    quantile = method_data['deterministic_duration']
    if quantile == 'mean':
        q = None
    else:
        q = int(quantile[:2]) / 100.
    if method_data['method'] == 'Deterministic':
        asg = ass.DeterministicAssignment(instance, crit,
                                      mip_quantile=q,
                                      strict_start=STRICT_START_DETERMINISTIC_ASG)
    elif method_data['method'] =='2-stage Stochastic':
        asg = ass.TwoStageStochasticAssignment(instance, crit,
                                               max_considered_emergencies=method_data['max_emergencies'],
                                               number_scenarios_for_setting_tentatives=method_data['scenarios_tentative'],
                                               timelimit=MIP_TIME_LIMIT)
    elif method_data['method'] == 'First-Fit':
        print('FIRSTFIT')
        asg = ass.FirstFitAssignment(instance, crit, quantile=q, capacity=method_data['capacity'])

    print('OPTIMIZATION TERMINATED. ASSIGNMENT:')
    print(asg.full_assignment)
    asg_data = {'assignment': asg.assignment, 'starts': asg.tentatives}
    return tuple_converter(asg_data)

@app.callback(
    Output(component_id='graph-container', component_property='children'),
    Input(component_id='scenario', component_property='value'),
    Input(component_id='current-time', component_property='value'),
    Input(component_id='instance_data', component_property='data'),
    Input(component_id='assignment_data', component_property='data'),
    Input(component_id='alpha', component_property='value'),
    Input(component_id='delta', component_property='value'),
    Input(component_id='delta2', component_property='value'),
)
def visualize(scenario_seed, ctime, instance_data, assignment_data, alpha, delta, delta2):
    ctx = callback_context
    if not ctx.triggered:
        raise PreventUpdate
    if None in (instance_data, assignment_data, alpha, delta, delta2):
        raise PreventUpdate



    scenario_seed = int(max(scenario_seed,0))

    instance, crit = get_instance_crit(instance_data)
    asg = get_assignment(instance, crit, assignment_data)
    bsp = pol.BlockSchedulingPolicy(instance, asg.full_assignment, asg.tentatives, pol.OnlineRule(alpha=alpha,
                                                                                                  delta=delta,
                                                                                                  delta2=delta2))
    scenario = instance.sample_scenario(do_seed=True, random_seed=scenario_seed)
    bsp.simulate_scenario(scenario)
    max_loads = [max([bsp.load[d, v] for d, v in bsp.load if d == d0]) for d0 in range(5)]
    d = int(ctime)
    if d==5:
        current_time = (4, None)  # final state
    else:
        h = ctime-d
        t = max_loads[d] * h
        current_time = (d,t)

    figs = bsp.visualize_plotly(current_time=current_time, scenario=scenario)
    nblocks = [len([1 for key,value in bsp.load.items() if key[0]==d and value>0]) for d in range(5)]

    # ----------------------- #
    #  Spider plot
    # ----------------------- #


    detail, total = bsp.get_cost(crit)
    r = []
    cost_name = []
    txt = []
    for k, v in detail.items():
        if k.endswith('_cost'):
            txt.append(f'   {k.replace("_", " ")}s: {v}')
            if 'scheduling' in k:
                k += '/10'
                v /= 10
            cost_name.append(k)
            r.append(v)

    df = pd.DataFrame(dict(costs=r, type=cost_name))
    spider = px.line_polar(df, r='costs', theta='type', line_close=True, start_angle=-54,
                        hover_data={'costs': False,
                                    'type': False,
                                    'category': txt},
                        color_discrete_sequence=px.colors.sequential.Plasma_r,
                        template="plotly_dark",
                        title=f"total costs= {total}"
                        )

    # -------------------------------- #
    # bar chart (status per specialty  #
    # -------------------------------- #

    specialties = sorted(instance.jobs.keys())
    nsp = len(specialties)
    labels = [sp[:3] for sp in specialties]
    num_status = {}
    for sta in ['scheduled', 'postponed', 'cancelled', 'rescheduled']:
        num_status[sta] = [0] * nsp
    for isp, sp in enumerate(specialties):
        for i in range(len(instance.jobs[sp])):
            num_status[bsp.status[sp, i]][isp] += 1

    status = []
    count = []
    specialty = []
    for k, kls in num_status.items():
        for i, c in enumerate(kls):
            status.append(k)
            count.append(c)
            specialty.append(specialties[i])
    df = pd.DataFrame(dict(status=status, count=count, specialty=specialty))

    bar = px.bar(df, x="specialty", y="count", color="status", title="Job status per specialty")



    figure_list_dash = html.Div([
        html.Div([dcc.Graph(figure=figs[d],
                  style={'width': '37vw', 'height': f'{3*nblocks[d]}vw'}) for d in range(2)]),
        html.Div([dcc.Graph(figure=figs[d],
                            style={'width': '37vw', 'height': f'{3*nblocks[d]}vw'}) for d in range(2,5)]),
        html.Div([dcc.Graph(figure=spider, style = {'width': '26vw', 'height': '26vw'}),
                  dcc.Graph(figure=bar, style={'width': '26vw', 'height': '26vw'})])
        ],
        style={'display': 'flex', 'flex-direction': 'row'}
    )


    return figure_list_dash


@app.callback(
    Output(component_id='simulation-container', component_property='children'),
    Input(component_id='simulation', component_property='n_clicks'),
    State(component_id='scenario-n', component_property='value'),
    State(component_id='instance_data', component_property='data'),
    State(component_id='assignment_data', component_property='data'),
    State(component_id='alpha', component_property='value'),
    State(component_id='delta', component_property='value'),
    State(component_id='delta2', component_property='value'),
)
def simulate(button, n, instance_data, assignment_data, alpha, delta, delta2):
    ctx = callback_context
    if not ctx.triggered:
        raise PreventUpdate
    if button is None or instance_data is None:
        raise PreventUpdate

    instance, crit = get_instance_crit(instance_data)
    asg = get_assignment(instance, crit, assignment_data)
    bsp = pol.BlockSchedulingPolicy(instance, asg.full_assignment, asg.tentatives, pol.OnlineRule(alpha=alpha, delta=delta, delta2=delta2))

    print('start simulation...')
    instance.build_train_test_sets(n, 0, do_seed=False)
    total_costs, detailed_costs, _, mean_costs = bsp.evaluate(crit, train=True)
    print('simulation done.')

    dcost = []
    types = []
    scenario = []
    scheduling = []

    Omega = np.argsort(total_costs)

    for i, sc in enumerate(Omega):
        costs = detailed_costs[sc]
        scheduling.append(costs['scheduling_cost'])
        for type in ['scheduling', 'idling', 'waiting', 'overtime', 'cancellation']:
            scenario.append(i)
            dcost.append(costs[type + '_cost'])
            types.append(type)

    df = pd.DataFrame(dict(scenario=scenario,
                           costs=dcost,
                           type=types))

    fig1 = px.area(df, x='scenario', y='costs', color='type')
    l = min(scheduling)
    u = max(total_costs)
    ll = max(l - 0.1 * (u - l), 0)
    uu = u + 0.1 * (u - l)

    fig1.update_yaxes(range=[ll,uu])
    fig1.update_layout(margin=dict(l=2, r=2, t=80, b=20))
    fig1.update_yaxes(visible=False, showticklabels=False)

    df2 = df.groupby('scenario').sum()
    fig2 = px.histogram(df2, y="costs", marginal="box", title=f"mean costs: {np.mean(total_costs)}")
    fig2.update_yaxes(range=[ll,uu])
    fig2.update_layout(margin=dict(l=2, r=2, t=80, b=20))

    dash_output = html.Div([dcc.Graph(figure=fig2, style={'width': '30vw', 'height': '26vw'}),
                            dcc.Graph(figure=fig1, style={'width': '40vw', 'height': '26vw'})],
                           style={'display': 'flex', 'flex-direction': 'row'})

    return dash_output

app.run_server(debug=False)
