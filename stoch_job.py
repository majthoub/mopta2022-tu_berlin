import scipy.stats as sst
import numpy as np
import math as mt
import numpy as np
import copy

from scipy.special import gammaincc

def Phi(x):
    return 0.5 * (1 + np.math.erf(x / 2 ** 0.5))


class StochJob(object):
    """
    A parent class for stochastic jobs
    """

    def __init__(self):
        self.scipy_obj = None
        self.mean = None
        self.var = None
        self.std = None

    @classmethod
    def from_dict(cls, dct):
        assert 'type' in dct
        if dct['type'] == 'lognormal':
            assert ('mu' in dct) and ('sigma' in dct)
            return LognormalJob(mu=dct['mu'], sigma=dct['sigma'])
        elif dct['type'] == 'uniform':
            assert ('a' in dct) and ('b' in dct)
            return UniformJob(a=dct['a'], b=dct['b'])
        elif dct['type'] == 'gamma':
            assert ('alpha' in dct) and ('beta' in dct)
            return GammaJob(alpha=dct['alpha'], beta=dct['beta'])
        elif dct['type'] == 'twopoints':
            assert ('a' in dct) and ('b' in dct) and ('p' in dct)
            a, b, p = dct['a'], dct['b'], dct['p']
            if b==a:
                return DeterministicJob(a=a)
            elif p==0:
                return DeterministicJob(a=b)
            elif p==1:
                return DeterministicJob(a=a)
            else:
                return TwoPointsJob(a=dct['a'], b=dct['b'], p=dct['p'])
        elif dct['type'] == 'deterministic':
            assert ('a' in dct)
            return DeterministicJob(a=dct['a'])
        elif dct['type'] == 'discrete':
            assert ('a' in dct) and ('p' in dct)
            return DiscreteJob(a=dct['a'], p=dct['p'])
        else:
            raise ValueError("Type of stochastic job not implemented yet: {0}".format(dct['type']))

    @classmethod
    def from_mean_var(cls, mean, var, s=0):
        """generates a random job from the desired child class,
           with mean and var parameters
           Additionally, some distributions accept a shape parameter s in [0,1]
        """
        pass

    @classmethod
    def from_3params(cls, par1, par2, par3):
        """generates a random job from 3 given parameters.
           This actually calls from_mean_var, unless the method is overriden
           in the corresponding class
        """
        return cls.from_mean_var(par1, par2, par3)

    def rvs(self, size=1):
        """generates a random realization of the stochastic job"""
        if size == 1:
            return self.scipy_obj.rvs()
        else:
            return self.scipy_obj.rvs(size=size)

    def rvs_largerthan(self, tau, q=None, acceptance_rejection=False):
        """generates a random realization of the stochastic job, conditionned to X>tau
           if q is provided, returns the realization corresponding to quantile q
        """
        assert tau >= 0
        if (q is None) and acceptance_rejection:
            t = -1
            while t <= tau:
                t = self.scipy_obj.rvs()
            return t
        else:
            if q is None:
                q = np.random.rand()        # q in [0,1]
            qtau = q + (1-q) * self.scipy_obj.cdf(tau)
            return self.scipy_obj.ppf(qtau) # ppf is inverse of cdf

    def EX_conditionned_X_larger_t(self, t, speed=1.):
        """
        conditional expectation of X/speed, conditioned by X/speed>=t
        """
        pass

    def truncate_larger_than(self, tau):
        """
        Return the job with random variable (X|X>tau)
        """
        pass

    def cdf(self, x):
        return self.scipy_obj.cdf(x)

    def to_dict(self):
        """
        returns a dictionary representing the variable
        """
        pass

    def expected_progress(self, t, speed=1.):
        return t / self.EX_conditionned_X_larger_t(t, speed=speed)

    def discretize(self, step, epsilon):
        cum_p = 0.
        low = 0
        a = []
        p = []
        while cum_p < 1 - epsilon:
            upp = low + step
            pint = self.cdf(upp) - self.cdf(low)
            a.append(upp)
            p.append(pint)
            cum_p += pint
            low = upp
        a = np.array(a)
        p = np.array(p)/cum_p

        return DiscreteJob(a,p)

class LognormalJob(StochJob):
    def __init__(self, mu, sigma):
        super(LognormalJob, self).__init__()
        self.mu = mu
        self.sigma = sigma

        self.mean = np.exp(mu + 0.5 * sigma ** 2)
        self.var = self.mean ** 2 * (np.exp(sigma ** 2) - 1)
        self.std = self.var ** 0.5
        self.scipy_obj = sst.lognorm(sigma, scale=np.exp(mu))

    @classmethod
    def from_mean_var(cls, mean, var, s=0):

        # shape parameter s is not used, assert=0
        assert s == 0

        sigma2 = np.log(1. + var / mean ** 2)
        mu = np.log(mean) - 0.5 * sigma2
        return cls(mu=mu, sigma=sigma2 ** 0.5)

    def rvs(self, size=1):
        """faster overrides using np.rand instead of scipy's rvs"""
        if size == 1:
            return np.exp(np.random.randn() * self.sigma + self.mu)
        else:
            return np.exp(np.random.randn(size) * self.sigma + self.mu)

    def EX_conditionned_X_larger_t(self, t, speed=1.):
        if t == 0:
            alpha = 1
            proba_X_less_t = 0.
        else:
            alpha = Phi((-np.log(t) + self.mu - np.log(speed) + self.sigma ** 2) / self.sigma)
            proba_X_less_t = Phi((np.log(t) - self.mu + np.log(speed)) / self.sigma)

        return np.exp(self.mu - np.log(speed) + self.sigma ** 2 / 2.) * alpha / (1 - proba_X_less_t)

    def truncate_larger_than(self, tau):
        raise NotImplementedError

    def to_dict(self):
        return {'type': 'lognormal', 'mu': self.mu, 'sigma': self.sigma}


class UniformJob(StochJob):
    def __init__(self, a, b):
        super(UniformJob, self).__init__()
        assert a <= b
        self.a = a
        self.b = b

        self.mean = (a + b) / 2.
        self.var = (b - a) ** 2 / 12.
        self.std = self.var ** 0.5
        self.scipy_obj = sst.uniform(loc=a, scale=b - a)

    @classmethod
    def from_mean_var(cls, mean, var, s=0):

        # shape parameter s is not used, assert=0
        assert s == 0

        a = mean - (3 * var) ** 0.5
        b = mean + (3 * var) ** 0.5
        assert a >= 0
        return cls(a, b)

    def rvs(self, size=1):
        """faster overrides using np.rand instead of scipy's rvs"""
        if size == 1:
            return self.a + np.random.rand() * (self.b - self.a)
        else:
            return self.a + np.random.rand(size) * (self.b - self.a)

    def EX_conditionned_X_larger_t(self, t, speed=1.):

        a = self.a / speed
        b = self.b / speed

        if t <= a:
            return self.mean
        elif t > b:
            raise ValueError("U([{0},{1}]) cannot be larger than {2}".format(a, b, t))
        else:
            return (b + t) / 2.

    def truncate_larger_than(self, tau):
        assert tau <= self.b
        if tau <= self.a:
            return UniformJob(self.a, self.b)
        else:
            return UniformJob(tau, self.b)

    def to_dict(self):
        return {'type': 'uniform', 'a': self.a, 'b': self.b}


class TwoPointsJob(StochJob):
    def __init__(self, a, b, p):
        super(TwoPointsJob, self).__init__()
        self.p = p
        self.a = a
        self.b = b

        self.mean = p * a + (1 - p) * b
        self.var = (b - a) ** 2 * p * (1 - p)
        self.std = self.var ** 0.5
        self.scipy_obj = sst.rv_discrete(name='twopoints', values=([self.a, self.b], (self.p, 1 - self.p)))

    @classmethod
    def from_mean_var(cls, mean, var, s=0):
        """
        The shape parameter s should be in (-1,1)
        The returned distribution will satisfy a = |s| b.
        For s!=0, there are 2 solutions, and the solution with the
        smallest p is selected if s>0 otherwise the solution with the largest p is selected.
        """
        if s > 1 or s < -1:
            raise ValueError('s should be between 0 and 1')

        # Assuming s=0
        if s == 0:
            b = mean + var / mean
            a = 0
            p = var / (mean ** 2 + var)
            return cls(a, b, p)
        elif abs(s) == 1:
            assert var == 0
            return DeterministicJob(mean)
        else:
            c = var / mean ** 2
            assert c <= 0.25 * (1. - abs(s)) ** 2 / abs(s)
            if s > 0:
                p = (2 * c + (1 - s) * (1 - (1 - 4 * s * c / (1 - s) ** 2) ** 0.5)) / (2 * (1 + c) * (1 - s))
            else:
                s = -s
                p = (2 * c + (1 - s) * (1 + (1 - 4 * s * c / (1 - s) ** 2) ** 0.5)) / (2 * (1 + c) * (1 - s))
            b = mean / (1. - (1 - s) * p)
            a = s * b
            return cls(a, b, p)

    @classmethod
    def from_mean_shape_p(cls,m,s,p):
        b = m/(1.-p*(1-s))
        a = s * b
        return cls(a,b,p)

    @classmethod
    def from_3params(cls, par1, par2, par3):
        """
        For TwoPointsJob, the 3params creator uses from_mean_shape_p instaead of from_mean_var
        """
        return cls.from_mean_shape_p(par1, par2, par3)

    def rvs(self, size=1):
        """faster overrides using np.rand instead of scipy's rvs"""
        if size == 1:
            return self.a + ((self.b - self.a) if np.random.rand() < 1 - self.p else 0.)
        else:
            return self.a + (self.b - self.a) * (np.random.rand(size) < 1 - self.p)

    def truncate_larger_than(self, tau):

        assert tau < max(self.a, self.b)

        if tau < min(self.a, self.b):
            return TwoPointsJob(self.a, self.b, self.p)
        else:
            return DeterministicJob(max(self.a, self.b))


    def EX_conditionned_X_larger_t(self, t, speed=1.):
        a = self.a / speed
        b = self.b / speed
        if t <= min(a, b):
            return self.mean
        elif t > max(a, b):
            raise ValueError("TwoPoints([{0},{1}]) cannot be larger than {2}".format(a, b, t))
        elif min(a, b) <= t <= max(a, b):
            return max(a, b)

    def to_dict(self):
        return {'type': 'twopoints', 'a': self.a, 'b': self.b, 'p': self.p}


class DeterministicJob(TwoPointsJob):
    def __init__(self, a):
        super().__init__(a, a, 1.)

    @classmethod
    def from_mean_var(cls, mean, var, s=0):
        # shape is not used, assert == 0
        assert s == 0
        assert var == 0
        return cls(mean)

    @classmethod
    def from_3params(cls, par1, par2, par3):
        """
        For TwoPointsJob, the 3params creator uses from_mean_shape_p instaead of from_mean_var
        """
        return cls.from_mean_var(par1, par2, par3)

    def rvs(self, size=1):
        """faster override"""
        if size == 1:
            return self.a
        else:
            return self.a * np.ones(size)

class GammaJob(StochJob):
    def __init__(self, alpha, beta):
        super(GammaJob, self).__init__()
        self.alpha = alpha
        self.beta = beta

        self.mean = alpha / beta
        self.var = alpha / beta ** 2
        self.std = self.var ** 0.5
        self.scipy_obj = sst.gamma(a=alpha, loc=0, scale=1. / beta)

    @classmethod
    def from_mean_var(cls, mean, var, s=0):

        # shape parameter s is not used, assert=0
        assert s == 0

        alpha = mean ** 2 / var
        beta = mean / var
        assert alpha > 0 and beta > 0
        return cls(alpha, beta)

    def EX_conditionned_X_larger_t(self, t, speed=1.):
        beta = self.beta * speed
        if t >= 0:
            return 1 / (1 - self.scipy_obj.cdf(t)) * (self.alpha / beta) * gammaincc(self.alpha + 1, beta * t)
        else:
            return self.mean

    def to_dict(self):
        return {'type': 'gamma', 'alpha': self.alpha, 'beta': self.beta}

    def truncate(self, tau):
        raise NotImplementedError


class DiscreteJob(StochJob):
    def __init__(self, a, p):
        super(DiscreteJob, self).__init__()
        self.a = np.array(a)
        p = np.array(p)

        assert abs(sum(p) - 1) <= 1e-6
        self.p = p/sum(p)

        self.mean = self.a.dot(self.p)
        self.var = (self.a ** 2).dot(self.p) - self.mean ** 2
        self.std = self.var ** 0.5
        self.scipy_obj = sst.rv_discrete(values=(self.a, self.p))
        self.cumulative_p = np.cumsum(self.p)

    @classmethod
    def random_job(cls, low_k=2, high_k=10, min_support_point=0, max_support_point=10):

        # number of support points
        k = np.random.randint(low=low_k, high=high_k)
        # support
        s = np.sort(np.random.choice(range(min_support_point, max_support_point+1), k, replace= False))

        pr = np.cumsum(sst.expon.rvs(size=k))
        pr = pr / np.sum(pr)
        np.random.shuffle(pr)
        pr = np.round(pr, 2)
        pr = np.maximum(pr, 0.01)

        sum_pr = sum(pr)
        while abs(sum_pr - 1) > 1e-6:
            if sum_pr >= 1.01 - 1e-6:
                i = np.random.randint(k)
                if pr[i] >= 0.02:
                    pr[i] -= 0.01
            if sum_pr <= 0.99 + 1e-6:
                i = np.random.randint(k)
                if pr[i] <= 0.98:
                    pr[i] += 0.01
            sum_pr = sum(pr)

        pr = np.round(pr, 2)
        assert abs(sum(pr) - 1) < 1e-6

        return cls(s, pr)

    def EX_conditionned_X_larger_t(self, t, speed=1.):
        aa = np.array(self.a)/speed
        if t > max(aa):
            raise ValueError("{0} cannot be larger than {1}".format(t , max(aa)))
        else:
            numerator = sum(a_i * p_i for a_i, p_i in zip(aa, self.p) if a_i >= t)
            denominator = sum(p_i for a_i, p_i in zip(aa, self.p) if a_i >= t)
        return numerator / denominator

    def to_dict(self):
        lista = [int(ai) for ai in self.a]
        listp = [float(p_i) for p_i in self.p]
        return {'type': 'discrete', 'a': lista, 'p': listp}

    def cdf(self, x):
        i = np.searchsorted(self.a, x + 1e-6)
        return np.r_[0, self.cumulative_p][i]

    def rvs(self, size=1):
        """faster overrides using np.rand instead of scipy's rvs"""
        if size == 1:
            r = np.random.rand()
            i = np.searchsorted(self.cumulative_p, r)
            return self.a[i]
        else:
            rr = np.random.rand(size)
            ii = np.searchsorted(self.cumulative_p, rr)
            return self.a[ii]

    def rvs_largerthan(self, tau, q=None, acceptance_rejection=False):
        """generates a random realization of the stochastic job, conditionned to X>tau
           if q is provided, returns the realization corresponding to quantile q
        """
        assert tau >= 0
        if (q is None) and acceptance_rejection:
            t = -1
            while t <= tau:
                t = self.rvs()
            return t
        else:
            if q is None:
                q = np.random.rand()  # q in [0,1]

            q = min(q, 1 - 1e-6)
            itau = np.searchsorted(self.a, tau + 1e-6)
            p_cond = copy.copy(self.p[itau:])  #
            p_cond /= sum(p_cond)  #
            p_cond_cum = np.cumsum(p_cond)
            i = np.searchsorted(p_cond_cum, q)
            # print(len(self.a),len(self.p),max(p_cond_cum),q)
            return self.a[itau + i]

    def truncate_larger_than(self, tau):

        assert tau < np.max(self.a)

        b = []
        q = []
        for i in range(len(self.a)):
            if self.a[i] > tau:
                b.append(self.a[i])
                q.append(self.p[i])

        b = np.array(b)
        q = np.array(q)
        q /= sum(q)

        return DiscreteJob(b, q)