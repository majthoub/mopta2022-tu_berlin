import picos

import scipy.stats as sst
import numpy as np
import json
import gurobipy as gp
from gurobipy import GRB, quicksum


class Assignment:
    """class used to optimize and visualize expected assignments"""

    def __init__(self, instance, criterion, assignment=None, starts=None):
        """
        assignment is a dict of lists:  {block -> [list of patients]}
            it gives the (initial) assignment of each job

        can be initialized to None and optimized with other function of a child class

        similarly for "tentatives" {specialty -> list of times or None}, which gives the (initial) tentative time of jobs
        """
        self.instance = instance
        if assignment is None:
            self.assignment = {}
        else:
            self.assignment = assignment

        if starts is None:
            self.tentatives = {}
        else:
            self.tentatives = starts

        self.criterion = criterion

    def get_block(self, sp, ii):
        """
        returns the block (day,index) where the ii-th patient of specialty `sp` is assigned to (or None if dummy block)
        """
        for block in self.instance.blocks[sp]:
            if ii in self.assignment[block]:
                return block
        return None

    @property
    def full_assignment(self):
        """
        returns the assignment as dictionary {specialty -> list of blocks or None}
        """
        asg = {}
        for sp in self.instance.blocks:
            asg[sp] = [None] * len(self.instance.jobs[sp])
        for sp in self.instance.blocks:
            for block in self.instance.blocks[sp]:
                for k in self.assignment[block]:
                    asg[sp][k] = block
        return asg

    def get_mean_scenario(self, quantile=None):
        """
        fill the variables `mean_start`, `mean_completion` and `mean_load`
        which contain the starting times and completion times of each job and the load of each machine,
        corresponding to a deterministic scenario with duration d_i = E[P_i] if quantile is None,
        or d_i= Quqntile(P_i,q) if 0<q<1.
        """
        self.mean_start = {}
        self.mean_completion = {}
        self.mean_load = {}
        for sp in self.instance.blocks:
            for block in self.instance.blocks[sp]:
                self.mean_load[block] = 0.

        for sp in self.instance.jobs:
            self.mean_start[sp] = [None] * len(self.instance.jobs[sp])
            self.mean_completion[sp] = [None] * len(self.instance.jobs[sp])
            for block in self.instance.blocks[sp]:
                for j in self.assignment[block]:
                    job = self.instance.jobs[sp][j]
                    self.mean_start[sp][j] = self.mean_load[block]
                    if quantile is None:
                        d = job.mean
                    else:
                        d = job.scipy_obj.ppf(quantile)
                    self.mean_load[block] += d
                    self.mean_completion[sp][j] = self.mean_load[block]

    def set_mean_starts_as_tentatives(self, quantile=None):
        """
        compute the mean scenario, and set the starting times in this mean scenario as the tentative starting times.
        """
        self.get_mean_scenario(quantile)
        for sp in self.instance.jobs:
            self.tentatives[sp] = self.mean_start[sp][:]


class FirstFitAssignment(Assignment):
    """class used to compute a patient-block assignment
       by first fit, with jobs sorted by mu_i/cib_2-cib_1
    """
    def __init__(self, instance, criterion,
                 quantile=None,
                 capacity=500):
        super().__init__(instance, criterion)
        self.quantile = quantile
        self.capacity=capacity

        self.compute_assignment()
        self.set_mean_starts_as_tentatives(quantile)

    def compute_assignment(self):
        for sp in self.instance.jobs:
            self.compute_assignment_specialty(sp)

    def compute_assignment_specialty(self, sp):
        """
        computes an assignment for the specialty `sp` by First-Fit (in order of adapted smith ratio mu_i/cib_2-cib_1)
        """
        days = list(set([d for d, _ in self.instance.blocks[sp]]))
        b0 = (days[0],1)
        if self.criterion.flowtime_unit == 'day':
            b1 = (days[1],1)
        else:
            b1 = None  # take the dummy block to compute the cost gradient

        # sort patient by cost_gradient/mean_duration
        tab = []
        for ij,job in enumerate(self.instance.jobs[sp]):
            w = self.criterion.get_c_ib((sp,ij), b1) - self.criterion.get_c_ib((sp,ij), b0)
            p = self.instance.jobs[sp][ij].mean
            tab.append(float(p)/w)

        # insert by first-fit
        blocks = self.instance.blocks[sp]

        for block in blocks:
            self.assignment.setdefault(block, [])

        loads = {b:0. for b in blocks}
        patients = np.argsort(tab)
        for ij in patients:
            Pj = self.instance.jobs[sp][ij]
            if self.quantile is None:
                dj = Pj.mean
            else:
                dj = Pj.scipy_obj.ppf(self.quantile)
            for b in blocks:
                if loads[b] + dj <= self.capacity:
                    loads[b] += dj
                    self.assignment[b].append(ij)
                    break


class DeterministicAssignment(Assignment):
    """class used to compute an optimal assignment
       resulting from a deterministic instance, in which random processing times
       are replaced with a deterministic duration (mean or quantile)
    """

    def __init__(self, instance, criterion,
                 mip_quantile=None,
                 strict_start=False):

        super().__init__(instance, criterion)

        # parameter used for the (simple) MIP_optimal solution, which replaces every duration with a deterministic value
        self.mip_quantile = mip_quantile
        self.strict_start = strict_start

        self.MIP_optimize()
        self.set_mean_starts_as_tentatives(mip_quantile)

    def MIP_optimize(self):
        for sp in self.instance.jobs:
            self.MIP_optimize_specialty(sp)

    def MIP_optimize_specialty(self, specialty):
        """
        if self.quantile is None, use the mean as job duration.
        otherwise, use this quantile for all jobs (quick & dirty robust solution)

        With `strict_start=True`, no job can start in overtime
        """
        quantile = self.mip_quantile
        m = len(self.instance.blocks[specialty]) + 1
        n = len(self.instance.jobs[specialty])
        P = picos.Problem()
        x = picos.BinaryVariable('x', (n, m))  # x[i,b]=1 if i assigned to block b
        if self.strict_start:
            z = picos.BinaryVariable('z', (n, m - 1))  # z[i,b]=1 if i assigned to block b, with completion in overtime
        capa = 480.
        cc = np.zeros((n, m))
        for k in range(n):
            patient = specialty, k
            for b, block in enumerate(self.instance.blocks[specialty] + [None]):
                cc[k, b] = self.criterion.get_c_ib(patient, block)
        ccc = picos.Constant('c', cc)
        y = picos.RealVariable('y', m - 1, lower=0)
        if quantile is None:
            d = picos.Constant('d', [job.mean for job in self.instance.jobs[specialty]])
        else:
            d = picos.Constant('d', [job.scipy_obj.ppf(quantile) for job in self.instance.jobs[specialty]])
        if not (self.strict_start):
            P += (picos.sum(x, axis=1) == 1)
            P += (d.T * x)[:-1] <= capa + y
            obj = (ccc | x) + self.criterion.overtime_cost * (1 | y)
        else:
            P += (picos.sum(x, axis=1) + picos.sum(z, axis=1) == 1)
            P += ((d.T * x)[:-1] <= capa)
            P += ((d.T * x)[:-1] + (d.T * z)[:] <= capa + y)
            P += (picos.sum(z, axis=0) <= 1)
            obj = (ccc | x) + (ccc[:, :-1] | z) + self.criterion.overtime_cost * (1 | y)

        P.objective = 'min', obj

        P.solve(solver='gurobi', verbosity=2, timelimit=6, primals=None)
        for block in self.instance.blocks[specialty]:
            self.assignment.setdefault(block, [])

        xv = x.np
        if self.strict_start:
            zv = z.np
        for ib in range(m - 1):
            Jx = np.where(xv.T[ib] >= 0.999)[0]
            block = self.instance.blocks[specialty][ib]
            self.assignment[block].extend(Jx)
            if self.strict_start:
                if len(z.np.shape) == 1:
                    zv = zv.reshape(z.size)
                Jx = np.where(zv.T[ib] >= 0.999)[0]
                self.assignment[block].extend(Jx)


class TwoStageStochasticAssignment(Assignment):
    """class used to compute an optimal assignment with the Two-stage stochastic Approach
    """

    def __init__(self, instance, criterion, max_considered_emergencies=0,
                 number_scenarios_for_setting_tentatives=500,
                 timelimit=None):

        super().__init__(instance, criterion)

        self.max_considered_emergencies = max_considered_emergencies
        self.number_scenarios_for_setting_tentatives = number_scenarios_for_setting_tentatives
        self.timelimit = timelimit
        self.MIP_optimize()
        self.get_tentatives()

    def MIP_optimize(self):
        max_considered_emergencies = self.max_considered_emergencies
        if max_considered_emergencies == 0:
            for sp in self.instance.jobs:
                self.MIP_optimize_specialty(sp)
        else:
            # compute the duration of dummy emergency jobs
            N = sst.poisson(self.instance.emergency_rates)
            probas_emergencies = [N.pmf(i) for i in range(1 + max_considered_emergencies)]
            probas_emergencies = np.array(probas_emergencies)
            probas_emergencies /= sum(probas_emergencies)
            proba_at_least = np.cumsum(probas_emergencies[::-1])[::-1]
            dummy_durations = self.instance.emergency_mean_std[0] * proba_at_least[1:]
            self.MIP_optimize_all(dummy_durations)

    def MIP_optimize_all(self, dummy_durations):
        """define and solve the MIP for all specialties together where we also have emergency dummy_durations:

        min sum{b in B}(sum{i in I[s]_b} x_ib * c_ib) + sum{b in B'} y_b

        st (1) sum{b in (B[s])} x_ib = 1                                                 ,for all i in I[s] , for all specialties s in S
           (2) sum{b in B_d}  z_jb = 1                                                   ,for all days d in D, for all j in J_d
           (3) L_b = sum{i in I[s]} x_ib[s] * mu_i + sum{j in J_d} z_jb * l_j            ,for all blocks B (not including the dummy block)
           (4) a_ks * L_b + b_ks <= y_b                                                  ,for all k in K[s], for all b in B[s], for all s in S
           (5) x_ib in {0,1}                                                             ,for all i in I, for all b in B
           (6) z_jb in {0,1}                                                             ,for all j in J, for all b in B'

        where:
        S    - set of specialties
        B    - set of all blocks also including the dummy block b'
        B[s] - set of blocks for specialty s including b'

        B'   - set of all block not including the dummy block b'
        B'[s]- set of blocks of specialty s not including b'
        B_d  - set of blocks open for day d (not including b')
        L_b  - load of a block (haw many minutes are filled with jobs)
        a_k, b_k - opcost values read from json fiile for specialty s
        J_d  - dummy jobs that have to be placed for day d
        l_j  - length of dummy job j
        mu_i - mean length of job i of specialty s
        b'   - dummy block b'

        x_ib = 1 , if we place patient i in block b
        z_jb = 1 , if we place dummy job j in block b

        """

        cw = self.criterion.waiting_cost
        ci = self.criterion.idling_cost
        co = self.criterion.overtime_cost


        if cw == 0 and ci == 0 and co == 1:
            opcosts = json.load(open('operative_costs/cw0_ci0_co1.json', 'r'))
        elif cw == 0.5 and ci == 0 and co == 1:
            opcosts = json.load(open('operative_costs/cw0.5_ci0_co1.json', 'r'))
        elif cw == 0.5 and ci == 0.25 and co == 1:
            opcosts = json.load(open('operative_costs/cw0.5_ci0.25_co1.json', 'r'))
        else:
            raise ValueError('There is no such scenario for the costs')

        number_linear_pieces = '3'

        nd = len(dummy_durations) # number of dummy emergency jobs per day
        n = {}  # number of patients:  n[s] - amount of patients in I[s] of specialty s
        m = {} # number of blocks:  m[s] of specialty s, including b'
        I = {}  # list of patients: I[s][i] - the i-th patients of specialty s
        Bs = {} # list of blocks (without b') of specialty s
        Bprime = {}  # list of blocks (including b') - Bprime[s] is the list of blocks where jobs of specialty s can be assigned
        J = {}   # J[d] is the list of dummy blocks for day d
        Bd = {}  # Bd[d] is the list of blocks available on day d
        B = list(self.instance.enumerate_block_indices()) # list of all (non-dummy) blocks
        cc = {}  # cost function:  cc[s] is a matrix, such that cc[s][i][b]- cost of putting ith patient of specialty s
                                                                                        # in  bth block of speciality s

        #following sets contain indices rather than elements
        I_ = {}
        Bprime_ = {}
        Bs_ = {}
        J_ = {}
        Bd_ = {}

        for specialty in self.instance.jobs:
            n[specialty] = self.instance.njobs[specialty]
            m[specialty] = len(self.instance.blocks[specialty]) + 1
            I[specialty] = [(specialty, i) for i in range(n[specialty])]
            Bs[specialty] = self.instance.blocks[specialty][:]
            Bprime[specialty] = self.instance.blocks[specialty][:] + ['dummy_block']
            I_[specialty] = list(range(len(I[specialty])))
            Bprime_[specialty] = list(range(len(Bprime[specialty])))
            Bs_[specialty] = list(range(len(Bs[specialty])))
            n_spec = n[specialty]
            m_spec = m[specialty]
            c = np.zeros((n_spec, m_spec))
            for k in range(n_spec):
                patient = specialty, k
                for b, block in enumerate(self.instance.blocks[specialty] + [None]):
                    c[k, b] = self.criterion.get_c_ib(patient, block)
            cc[specialty] = c

        for d in range(5):
            J[d] = [('dummy', (d,k)) for k in range(nd)]
            Bd[d] = list(self.instance.enumerate_block_day(d))
            J_[d] = list(range(len(J[d])))
            Bd_[d] =list(range(len(Bd[d])))

        B_ = list(range(len(B)))
        K = list(range(int(number_linear_pieces)))

        # Define the MIP
        mo = gp.Model("full_mip")

        # Add Variables x:
        x = {}
        for sp in self.instance.jobs:
            x_specialty = mo.addVars(I_[sp], Bprime_[sp], vtype=GRB.BINARY, name=f'x[{sp}]')
            x[sp] = x_specialty

        # Add Variables z:
        z = {}
        for d in range(5):
            z_d = mo.addVars(J_[d], Bd_[d], vtype=GRB.BINARY, name=f'z[{d}]')
            z[d] = z_d

        # Add variables y:
        y = mo.addVars(B_, vtype=GRB.CONTINUOUS, name='y')

        # The Objective function
        # min sum{b in B}(sum{i in I[s]_b} x_ib * c_ib) + sum{b in B'} y_b
        mo.setObjective(quicksum(
            [quicksum([x[s][i,b] * cc[s][i,b] for i in I_[s] for b in Bprime_[s]])
             for s in self.instance.jobs]
        ) + quicksum([y[b] for b in B_]), GRB.MINIMIZE)

        # Constraint that forces each patient of a specialty to go to one of the blocks
        # (1) sum{b in (B[s])} x_ib = 1
        for s in self.instance.jobs:
            for i in I_[s]:
                mo.addConstr(quicksum(x[s][i, b] for b in Bprime_[s]) == 1)

        # (2) sum{b in B_d}  z_jb = 1
        for d in range(5):
            for j in J_[d]:
                mo.addConstr(quicksum(z[d][j, b] for b in Bd_[d]) == 1)

        # Constraints (3) and (4) together
        # (3) L_b = sum{i in I[s]} x_ib[s] * mu_i + sum{j in J_d} z_jb * l_j            ,for all blocks B (not including the dummy block)
        # (4) a_k * L_b + b_k <= y_b                                                    ,for all k in K[s], for all b in B[s], for all s in S

        for b in B_:
            block = B[b]
            d = block[0]
            s = self.instance.get_block_specialty(block)
            bs = Bs[s].index(block) #block index inside specialty
            bd = Bd[d].index(block) #block index inside day

            Lb = (quicksum([x[s][i, bs] * self.instance.jobs[s][i].mean for i in I_[s]]) +
                      quicksum([z[d][j,bd] * dummy_durations[j] for j in J_[d]]))


            for k in K:
                mo.addConstr(y[b] >= opcosts[s]["a"][number_linear_pieces][k] * Lb + opcosts[s]["b"][number_linear_pieces][k])

        mo.setParam('TimeLimit', self.timelimit)
        mo.optimize()

        #convert the gurobi-solution to an array
        # R[s][i][b] = 1, represents patient i being placed to block b for specialty s
        # EME[d][j][b] = 1, represents emergency dummy job j of day d placed to block b
        R = {}
        EME = {}
        for s in self.instance.jobs:
            # convert the gurobi-solution to an array r
            r = np.zeros((n[s], m[s]))
            for i in I_[s]:
                for b in Bprime_[s]:
                    r[i,b] = x[s][i, b].X
            R[s] = r

            for block in self.instance.blocks[s]:
                self.assignment.setdefault(block, [])
            xv = r
            for ib in range(m[s] - 1):
                Jx = np.where(xv.T[ib] >= 0.999)[0]
                block = self.instance.blocks[s][ib]
                self.assignment[block].extend(Jx)

        self.sort_assignment_by_variance()



    def MIP_optimize_specialty(self, specialty):
        """define and solve the MIP for a specialty:
        min sum{i in I, b in B} x_ib * c_ib + sum{b in B} y_b

        st (1) sum{b in B} x_ib = 1     for all i in I
           (2) y_b >= a_k * L_b + b_k
           (3) L_b = sum{i in I} x_ib * mu_i for all b
           (4) x in {0,1}

        where:
        i is the patient in I
        b is the block in B
        x_ib = 1 if we put patient i to block b
        L_b is the load in block b
        a_k, b_k are the variables in the dictionary in each speciality
        """

        m = len(self.instance.blocks[specialty]) + 1
        n = len(self.instance.jobs[specialty])

        cw = self.criterion.waiting_cost
        ci = self.criterion.idling_cost
        co = self.criterion.overtime_cost

        if cw == 0 and ci == 0 and co == 1:
            opcosts = json.load(open('operative_costs/cw0_ci0_co1.json', 'r'))
        elif cw == 0.5 and ci == 0 and co == 1:
            opcosts = json.load(open('operative_costs/cw0.5_ci0_co1.json', 'r'))
        elif cw == 0.5 and ci == 0.25 and co == 1:
            opcosts = json.load(open('operative_costs/cw0.5_ci0.25_co1.json', 'r'))
        else:
            raise ValueError('There is no such scenario for the costs')

        cc = np.zeros((n, m))
        for k in range(n):
            patient = specialty, k
            for b, block in enumerate(self.instance.blocks[specialty] + [None]):
                cc[k, b] = self.criterion.get_c_ib(patient, block)

        amount = '3'
        # amount='5'
        # amount='7'

        K = list(range(int(amount)))

        I = list(range(n))          # list of patients
        B = list(range(m))          # list of all blocks
        Bprime = list(range(m-1))   # all 'real' blocks (no dummy block)

        A = [(i, b) for i in I for b in B]
        # Create a new model
        mo = gp.Model("mip")

        # Create variables
        x = mo.addVars(A, vtype=GRB.BINARY, name="x")
        y = mo.addVars(Bprime, vtype=GRB.CONTINUOUS, name="y")

        # Set objective
        mo.setObjective(quicksum(x[a] * cc[a] for a in A) + quicksum(y[b] for b in Bprime), GRB.MINIMIZE)

        # Constraint (1)
        for i in range(n):
            mo.addConstr(quicksum(x[i, b] for b in B) == 1)

        # Constraint (3) and (4) together
        for b in range(len(Bprime)):
            for k in range(len(K)):
                mo.addConstr(y[b] >= opcosts[specialty]["a"][amount][k] * quicksum(x[i, b] * self.instance.jobs[specialty][i].mean for i in I) +
                             opcosts[specialty]["b"][amount][k])
        mo.optimize()

        # convert the gurobi-solution to an array r
        r = np.zeros((n, m))
        for i in range(n):
            for j in range(m):
                r[i][j] = x[i, j].X

        for block in self.instance.blocks[specialty]:
            self.assignment.setdefault(block, [])
        xv = r
        for ib in range(m - 1):
            Jx = np.where(xv.T[ib] >= 0.999)[0]
            block = self.instance.blocks[specialty][ib]
            self.assignment[block].extend(Jx)

        self.sort_assignment_by_variance()

    def sort_assignment_by_variance(self):
        for b in self.assignment:
            s = self.instance.get_block_specialty(b)
            self.assignment[b] = sorted(self.assignment[b], key=lambda i: self.instance.jobs[s][i].var)

    def get_tentatives(self):
        self.tentatives = {}
        for sp in self.instance.jobs:
            self.tentatives[sp] = [None] * self.instance.njobs[sp]
            for block in self.instance.blocks[sp]:
                jobs = []
                for j in self.assignment[block]:
                    job = self.instance.jobs[sp][j]
                    jobs.append(job)
                n = len(jobs)
                if n != 0:
                    K = self.number_scenarios_for_setting_tentatives
                    model = self.create_2dstage_LP_model(jobs, K)
                    # Optimize model
                    model.setParam('OutputFlag', 0)
                    model.optimize()
                    times = model.getAttr(GRB.Attr.X, model.getVars())[n * K: n * K + n]
                    for ij, j in enumerate(self.assignment[block]):
                        self.tentatives[sp][j] = times[ij]
                    print('____________', times, '_____________________')

    def create_2dstage_LP_model(self, jobs, K):
        n = len(jobs)
        p = np.array([job.rvs(size=K) for job in jobs])
        # Create operative cost model
        model = gp.Model('lp')
        T = 480  # overtime
        # Create variables
        s = model.addVars(n, K, name='s')
        t = model.addVars(n, name='t')
        ovt = model.addVars(K, name='ovt')
        # Set objective
        load = []
        obj = 0
        for k in range(K):
            load.append(s[n - 1, k] + p[n - 1, k])
            scenario_cost = 0
            for i in range(n):
                scenario_cost += self.criterion.waiting_cost * (s[i, k] - t[i])
            scenario_cost += self.criterion.idling_cost * (load[k] - sum(p.T[k]))
            scenario_cost += self.criterion.overtime_cost * ovt[k]
            obj += 1. / K * scenario_cost
        model.setObjective(obj, GRB.MINIMIZE)
        # Add Constraints
        for k in range(K):
            model.addConstr(ovt[k] >= load[k] - T, name='overtime ' + str(k))

            for i in range(n):
                model.addConstr(s[i, k] - t[i] >= 0, name='t (' + str(k) + ',' + str(i) + ')')
                if i >= 1:
                    model.addConstr(s[i, k] - s[i - 1, k] >= p[i - 1, k],
                                    name='p (' + str(k) + ',' + str(i) + ')')

        return model
