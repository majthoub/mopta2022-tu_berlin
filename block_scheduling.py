import numpy as np
import json
import stoch_job as sj
import scipy.stats as sst


class Criterion:
    """
    Defines an objective function of the form
      F = sum_{i in P, b in B} x_{ib} c_{ib}
        + sum_{i in P0} c^w  (S_i - t_i)_+
        + sum_{i in P0} c^i  (S_i - C_{i-(b)})_+
        + sum_{b in B\b'} c^o  (C_b - L_b)_+
        + c^c {number of job cancellation / postponing}

    where we use the following realization-dependent quantities
     * x_{ib} = {0,1}-assignment matrix of job i to block b (or dummy b')
     * P0    = set of jobs assigned to a non-dummy block
     * S_i    = Starting time of job i
     * C_{i-(b)} = Completion time of job preceeding i on the block where i was performed
     * C_b     = Latest completion of a job on block b
     * t_i = tentative starting time of job i

    and the following parameters define the criterion:
     * c[i,b] = cost of surgery i in block b, defined as c_{ib} = w_i * (day(b)+entry(i))**k, where
            _ entry(i)>0 indicates since how long i has been diagnosed, a random number in [0,entry_max(s)]
            _ w(i) is a patient-related weight, equal to a w0 * w(s) * random-integer(1...wmax)
            _ day(b) is the day of the block in {0,...,4} for non-dummy blocks,
            _ day(b') is a penalty set to get an interesting instance -> 2/w0+3 seems reasonable
            _ k is some exponent (defaults to 2)

        The max-entry coefs default to {'ORTH': 7, 'CARD': 7, 'GASTRO': 7, 'GYN': 7, 'URO': 7, 'MED': 7}
        The specialty-coefs w(s) default to {'ORTH': 1., 'CARD': 1., 'GASTRO': 1., 'GYN': 1., 'URO': 1., 'MED': 1.}
        The default wmax defaults to 4

     * c^w    = cost of waiting
     * c^i    = cost of idling
     * c^o    = cost of overtime [default to 1 and should not be changed; this serves as reference]
     * c^c    = cast of cancellation (one job can be cancelled on several days)
    """

    def __init__(self,
                 instance,
                 flowtime_unit='day',  # or 'week'
                 waiting_cost=0.35,
                 idling_cost=0.2,
                 cancellation_cost=120,  # same as 2 hours of overtime
                 w0=1.,
                 dummy_penalty=None,
                 max_entry=None,
                 wmax=4,
                 k_exp=2,
                 seed=None,
                 do_seed=False
                 ):
        self.instance = instance
        assert flowtime_unit in ('day', 'week')
        self.flowtime_unit = flowtime_unit
        self.waiting_cost = waiting_cost
        self.idling_cost = idling_cost
        self.overtime_cost = 1.
        self.cancellation_cost = cancellation_cost

        self.w0 = w0
        self.dummy_penalty = dummy_penalty
        if self.flowtime_unit == 'day':
            if self.dummy_penalty is None:
                self.dummy_penalty = 8# 2./w0 + 4.   #0.857 / w0 + 3.57
            if max_entry is None:
                self.max_entry = {'ORTH': 7, 'CARD': 7, 'GASTRO': 7, 'GYN': 7, 'URO': 7, 'MED': 7}
        else:
            if self.dummy_penalty is None:
                self.dummy_penalty = 1.
            if max_entry is None:
                self.max_entry = {'ORTH': 2, 'CARD': 2, 'GASTRO': 2, 'GYN': 2, 'URO': 2, 'MED': 2}
        self.wmax = wmax
        self.k_exp = k_exp
        self.w = {}
        self.entry = {}

        if do_seed:
            np.random.seed(seed)

        for sp in self.instance.jobs:
            self.w[sp] = []
            self.entry[sp] = []
            for _ in self.instance.jobs[sp]:
                self.w[sp].append(self.w0 * np.random.randint(1, wmax + 1))
                self.entry[sp].append(np.random.randint(1,self.max_entry[sp]+1))

    def get_c_ib(self, patient, block):
        """
        patient is of form (specialty, k)
        block is of form (day, room)
        """
        sp, k = patient
        patient_weight = self.w[sp][k]
        patient_entry = self.entry[sp][k]
        Delta = self.max_entry[sp]
        if block is not None:
            d, r = block
            if self.flowtime_unit == 'week':
                d = 0
            return patient_weight * (d + patient_entry) ** self.k_exp
        else:
            return patient_weight * (self.dummy_penalty + patient_entry) ** self.k_exp

class Scenario:
    """
    defines a realization of the uncertainty for a BlockSchedulingInstance:
      * `electives` is a dict of lists   { specialty -> [d0, d1, ..., d_{n(s)} ]]}
      * `emergencies` is a dict of lists { day ->  [(posterior0,d0), (posterior1,d1),...]
    """

    def __init__(self,
                 electives,
                 emergencies):
        self.electives = electives
        self.emergencies = emergencies


class BlockSchedulingInstance:
    """
    This class is a container for all parameters defining an instance of
    stochastic machine scheduling on parallel identical machines

    * jobs: dict of elective stochastic jobs { specialty -> [job1,job2,...,job_{n(s)}] }
    * blocks: dict of available blocks: {specialty -> [(day1,index1), (day2,index2),... ] }

    * emergency_rates: : Poisson param (#cases / day),

    * discretization: 0 (lognormals) or discretization step in (5, 10,15,20,30)

    * delta_CV: coefficient of reduction of squared coef of variation (to sample a posterior for emergencies)

    """

    def __init__(self,
                 jobs,
                 blocks,
                 emergency_rates,
                 emergency_mean_std,
                 discretization,
                 delta_CV,
                 discretization_epsilon=0.001
                 ):

        self.jobs = jobs
        self.blocks = blocks
        self.emergency_rates = emergency_rates
        self.emergency_mean_std = emergency_mean_std
        self.discretization = discretization
        # neglect processing times after 1-epsilon quantile when discretizing
        self.discretization_epsilon = discretization_epsilon
        self.delta_CV = delta_CV
        self.njobs = {specialty: len(self.jobs[specialty])
                      for specialty in self.jobs}

    @staticmethod
    def create_job(mean, var, discretization, epsilon):
        assert discretization in (0, 5, 10, 15, 20, 30)
        if discretization == 0:
            return sj.LognormalJob.from_mean_var(mean, var)
        else:
            ln = sj.LognormalJob.from_mean_var(mean, var)
            return ln.discretize(discretization, epsilon)

    @staticmethod
    def get_posterior(mean, std, delta_CV, discretization, epsilon):
        """
        returns a lognormal job such that the marginal given mean and standard variation,
                but coefficient of variation has been reduced by factor delta_CV
        """
        CV = (std / mean) ** 2
        assert 0 < delta_CV <= 1
        alpha = delta_CV * CV
        beta = (1 - delta_CV) / (1 + delta_CV * CV)
        sigma2 = np.log(1. + beta * std ** 2 / mean ** 2)
        mu = np.log(mean) - 0.5 * sigma2
        if delta_CV < 1:
            M = sst.lognorm(sigma2 ** 0.5, scale=np.exp(mu))
            mm = M.rvs()
        else:
            mm = mean
        vv = alpha * mm ** 2
        return BlockSchedulingInstance.create_job(mm, vv, discretization, epsilon)

    @classmethod
    def from_seed(cls,
                  I,  # number of patients
                  emergency_rate,
                  random_seed,
                  discretization=0,
                  delta_CV=0.25,
                  discretization_epsilon=0.001):
        """
        creates an instance for the MOPTA competition
        delta_CV is the factor of reduciton of squared coef of variation (from block-prior to patient-posterior)
        """
        # seed RNG
        np.random.seed(random_seed)

        # blocks given as pair (day, OR)
        blocks = {'GASTRO': [(0, 1), (1, 1), (2, 1), (2, 2), (3, 2), (4, 2)],
                  'ORTH': [(0, 4), (1, 4), (1, 5), (2, 10), (3, 4), (4, 4)],
                  'CARD': [(0, 3), (0, 9), (2, 3), (4, 3), (4, 9)],
                  'GYN': [(0, 6), (1, 6), (1, 7), (2, 6), (2, 7), (3, 6), (3, 7), (4, 7)],
                  'URO': [(0, 8), (0, 10), (1, 8), (2, 9), (3, 8), (4, 8)],
                  'MED': [(2, 5)]
                  }

        # TODO is it OR-time or surgery time (cleanup ?)
        mean_std = {'CARD': (99, 53),
                    'GASTRO': (132, 76),
                    'GYN': (78, 52),
                    'MED': (75, 72),
                    'ORTH': (142, 58),
                    'URO': (72, 38),
                    }

        emergency_mean_std = (90, 70)

        assert I in (70, 100, 140, 200)

        njobs = {'CARD': int(np.round(0.14 * I)),
                 'GASTRO': int(np.round(0.18 * I)),
                 'GYN': int(np.round(0.28 * I)),
                 'MED': int(np.round(0.05 * I)),
                 'ORTH': int(np.round(0.17 * I)),
                 'URO': int(np.round(0.18 * I))}

        if I == 70:  # fix rounding error
            njobs['MED'] -= 1
            njobs['GYN'] -= 1

        jobs = {}
        for specialty in njobs:
            jb = []
            mean, std = mean_std[specialty]
            for k in range(njobs[specialty]):
                job = cls.get_posterior(mean, std, delta_CV, discretization, discretization_epsilon)
                job.id = (specialty, k)
                jb.append(job)
            jobs[specialty] = jb

        instance = cls(jobs, blocks, emergency_rate, emergency_mean_std, discretization, delta_CV,
                       discretization_epsilon=discretization_epsilon)
        instance.mean_std = mean_std
        return instance

    def sample_scenario(self,
                        do_seed=False,
                        random_seed=None
                        ):
        """
        samples a random scenario; it is possible to seed the RNG beforehand:
        """

        if do_seed:
            np.random.seed(random_seed)

        elective_durations = {}
        for specialty in self.jobs:
            d = []
            for j in self.jobs[specialty]:
                d.append(j.rvs())
            elective_durations[specialty] = d

        emergencies = {}
        P = sst.poisson(self.emergency_rates)
        mean, std = self.emergency_mean_std
        for day in range(5):
            arrivals = P.rvs()
            day_emerg = []
            for k in range(arrivals):
                X = type(self).get_posterior(mean, std, self.delta_CV, self.discretization, self.discretization_epsilon)
                X.id = ('EMER', day, k)
                day_emerg.append((X, X.rvs()))
            emergencies[day] = day_emerg

        return Scenario(elective_durations, emergencies)

    def build_train_test_sets(self, n_train, n_test, do_seed=False, random_seed=None):
        if do_seed:
            np.random.seed(random_seed)
        self.train_set = []
        self.test_set = []

        for i in range(n_train):
            self.train_set.append(self.sample_scenario())
        for i in range(n_test):
            self.test_set.append(self.sample_scenario())

    def enumerate_job_indices(self):
        for sp in self.jobs:
            for k in range(len(self.jobs[sp])):
                yield (sp, k)

    def enumerate_block_indices(self):
        for sp in self.blocks:
            for d, r in self.blocks[sp]:
                yield (d, r)

    def enumerate_block_day(self, d):
        for day,r in self.enumerate_block_indices():
            if day == d:
                yield (d,r)

    def get_block_specialty(self, block):
        for sp in self.jobs:
            if block in self.blocks[sp]:
                return sp
        raise ValueError('this block does not exist')
