import numpy as np
from collections import OrderedDict
import block_scheduling as bs
import plotly.express as px
import pandas as pd

class Event:
    """
    An event object for the discrete event simulation
    """
    def __init__(self,
                 type,
                 t,
                 r=None,
                 j=None,
                 d=None
                 ):
        self.type = type  # completion | release | stop
        self.t = t  # time of the event
        self.r = r  # OR of the event
        self.d = d  # day of event
        self.j = j  # job that completes / get released, or None if dummy job completion at start of day
        assert self.type in ('completion', 'release', 'stop')

    def __str__(self):
        return f'{self.type} event at d={self.d}, t = {self.t} for job {self.j} on OR {self.r}'


class EventQueue:
    """
    stores a queue of event distributed over days and rooms:
        queue[d][r] is the (sorted) list of events on day d and room r
        global_queue stores all events in sorted time
    """

    def __init__(self):
        self.queues = {}
        for d in range(5):
            self.queues[d] = {}
        self.global_queue = []
        self.all_event_times = []

    def insert(self, event):
        d = event.d
        r = event.r
        if r is not None:
            self.queues[d].setdefault(r, [])
            event_times = [ev.t for ev in self.queues[d][r]]
            pos = np.searchsorted(event_times, event.t)
            self.queues[d][r].insert(pos, event)
        global_pos = np.searchsorted(self.all_event_times, 10000 * event.d + event.t)
        self.global_queue.insert(global_pos, event)
        self.all_event_times.insert(global_pos, 10000 * event.d + event.t)

    def pop(self):
        ev = self.global_queue.pop(0)
        self.all_event_times.pop(0)
        d, r = ev.d, ev.r
        if r is not None:
            assert ev is self.queues[d][r][0]
            event = self.queues[d][r].pop(0)
        else:
            event = ev
        return event

    def get_event_pos(self, d, r, k):
        event = self.queues[d][r][k]
        epsilon = 1e-6
        pos = np.searchsorted(self.all_event_times, 10000 * event.d + event.t - epsilon)
        found = False
        while not found:
            ev = self.global_queue[pos]
            if ev is event:
                return pos
            else:
                pos += 1
            assert pos < len(self.global_queue), 'event not found in global queue'  # TODO ASK

    def remove(self, d, r, k):
        pos = self.get_event_pos(d, r, k)
        del self.global_queue[pos]
        del self.all_event_times[pos]
        del self.queues[d][r][k]  #


class BlockSchedulingPolicy:
    """
    This class stores a policy and can simulate a scenario
    """

    def __init__(self,
                 instance,
                 assignment,
                 starts,
                 online_rule
                 ):
        """
        assignment is a dict of lists:  {specialty -> [list of blocks]}
            it gives the (initial) assignment of each job, to either
            a block (d,r) on day d and room r, or `None` for the dummy block

        starts is a dict of lists: {specialty -> [list of tentative starts]}
            with start=`None` for jobs that are assigned to a dummy block

        online_rule is an instance of OnlineRule, which specifies
            how to handle the uncertainty (can postpone/cancel operations, insert emergencies)
        """
        self.instance = instance
        self.assignment = assignment
        self.tentative_starts = starts
        self.online_rule = online_rule
        self.online_rule.policy = self
        self.debug = False  # set to true to display debug messages

    def initialize_queue(self, until):
        self.expload = {}
        self.queue = EventQueue()
        for sp in self.instance.blocks:
            for d, r in self.instance.blocks[sp]:
                self.queue.queues[d].setdefault(r, [])
        for specialty in self.instance.blocks:
            for ii, t in enumerate(self.tentative_starts[specialty]):
                if t is not None:
                    if self.assignment[specialty][ii] is not None:
                        d, r = self.assignment[specialty][ii]
                        self.queue.insert(Event('release', t, r, (specialty, ii), d))
                        self.expload.setdefault((d, r), 0)
                        self.expload[d, r] = max(self.expload[d, r], t + self.instance.jobs[specialty][ii].mean)
        if until is not None:
            d,t = until
            self.queue.insert(Event('stop', t, d=d))


    def initialize_schedule(self):
        # those variables are used to evaluate the criterion
        self.assigned = {j: None for j in self.instance.enumerate_job_indices()}
        self.start = {j: None for j in self.instance.enumerate_job_indices()}
        self.completion = {j: None for j in self.instance.enumerate_job_indices()}
        self.delay = {j: None for j in self.instance.enumerate_job_indices()}
        self.postponed = {j: 0 for j in self.instance.enumerate_job_indices()}
        self.idle = {b: 0 for b in self.instance.enumerate_block_indices()}
        self.load = {b: 0 for b in self.instance.enumerate_block_indices()}

        # this variable is used for visualization:
        # OPs are sorted as scheduled | rescheduled | postponed | cancelled
        self.status = {j: None for j in self.instance.enumerate_job_indices()}

    def evaluate(self,
                 criterion,
                 scenarios=None,
                 montecarlo=None,
                 train=None,
                 test=None):
        """
        evaluate the criterion over one or several scenarios;
        exactly one of the arguments should not be None
            * scenarios -> costs for a single scenario or a list of scenarios
            * montecarlo -> MC simulation on `montecarlo` samples
            * train -> evaluate on train dataset
            * test -> evaluate on test dataset

        returns: list of total costs, dict of detailed average costs, average total cost
        """

        assert ((scenarios is not None) + (montecarlo is not None) +
                (train is not None) + (test is not None) == 1)

        if scenarios is not None:
            if not isinstance(scenarios, list):
                scenarios = [scenarios]
        elif montecarlo is not None:
            assert isinstance(montecarlo, (int, np.integer))
            scenarios = [self.instance.sample_scenario() for _ in range(montecarlo)]
        elif train is not None:
            scenarios = self.instance.train_set
        elif test is not None:
            scenarios = self.instance.test_set

        costs = []
        detailed_costs = []
        for sc_index, sc in enumerate(scenarios):
            self.simulate_scenario(sc)
            detail, cost = self.get_cost(criterion)
            costs.append(cost)
            detailed_costs.append(detail)

        mean_detailed_costs = {}
        for k in detailed_costs[0]:
            mean_detailed_costs[k] = np.mean([det[k] for det in detailed_costs])
        mean_details = OrderedDict(sorted(mean_detailed_costs.items(), key=lambda kv: (kv[0].endswith('_cost'), kv[0])))
        return costs, detailed_costs, np.mean(costs), mean_details

    def get_cost(self, criterion):
        details = {'postponing': 0.,
                   'scheduling': 0.,
                   'waiting': 0.,
                   'idling': 0.,
                   'overtime': 0.,
                   'cancellation': 0.
                   }
        for j in self.instance.enumerate_job_indices():
            asg = self.assigned[j]
            if asg is None:
                details['postponing'] += criterion.get_c_ib(j, None)
            else:
                details['scheduling'] += criterion.get_c_ib(j, asg)
                details['waiting'] += self.delay[j]
            details['cancellation'] += self.postponed[j]
        for b in self.instance.enumerate_block_indices():
            details['idling'] += self.idle[b]
            details['overtime'] += max(self.load[b] - 480, 0)

        details['waiting_cost'] = details['waiting'] * criterion.waiting_cost
        details['cancellation_cost'] = details['cancellation'] * criterion.cancellation_cost
        details['idling_cost'] = details['idling'] * criterion.idling_cost
        details['overtime_cost'] = details['overtime'] * criterion.overtime_cost
        details['scheduling_cost'] = details['postponing'] + details['scheduling']

        details = OrderedDict(sorted(details.items(), key=lambda kv: (kv[0].endswith('_cost'), kv[0])))
        return details, sum([details[k] for k in details if k.endswith('_cost')])

    def simulate_scenario(self, scenario, until=None):
        """
        Upon completion of a call to this function,
        the following dictionaries will be defined:
            * self.assigned[j]: in which block=[(d,r) | None] was patient j=(sp,k) assigned?
            * self.start[j]: at what time (or None) did patient j's operation start?
            * self.completion[j]: at what time (or None) did patient j's operation complete?
            * self.delay[j]: how much waiting-time did patient j cause ?
            * self.postponed[j]: how often was patient j's operation postponed ?
            * self.idle[b]: How much idle time was there in block b=(d,r)
            * self.load[b]: Latest completion time in block b
            * self.status[j]: status of j's operation in {'scheduled','rescheduled', 'postponed', 'cancelled'}

        if until is None, the scenario is simulated completely.
        Otherwise, a `stop` event is generated at t=`until`. The returned simulation is a picture of
        the current expected loads at this time.
        """
        # initialize the event queue and the empty schedule
        self.initialize_queue(until)
        self.initialize_schedule()
        # the simulation maintains an event queue and a pool of emergency on each day
        for sp, k in self.status:
            if self.assignment[sp][k] is None:
                self.status[sp, k] = 'postponed'

        self.simulation_stopped = False
        for d in range(5):
            self.simulate_day(d, scenario)
        try:
            assert all([sta is not None for sta in self.status.values()]), "a job has no status"
        except:
            nsc = [job for job in self.status if self.status[job] is None]
            import pdb;pdb.set_trace()

    def simulate_day(self, day, scenario):
        self.initialize_day(day)
        if self.simulation_stopped: #this must be a day after the stop -> consider `rates` basic emergencies
            emean,estd = self.instance.emergency_mean_std
            dis = self.instance.discretization
            eps = self.instance.emergency_mean_std
            em = type(self.instance).get_posterior(emean,estd,1,dis,eps)
            self.emergency_pool = {k: (em,em.mean) for k in range(int(self.instance.emergency_rates))}
        else:
            self.emergency_pool = {k: em for k, em in enumerate(scenario.emergencies[day])}
        t = 0
        while any(self.queue.queues[day].values()):
            if self.simulation_stopped:
                self.planned_schedule_for_remaining_of_day(day,t,scenario)
                return
            event = self.queue.pop()
            t = event.t
            r = event.r
            j = event.j
            d = event.d
            assert d == day
            if self.debug:
                print(event)
            if event.type == 'release':
                self.released_but_not_started[r].append(j)
                if self.available_block[r]:
                    self.make_one_step(event, scenario)

            elif event.type == 'completion':
                self.available_block[r] = True
                self.make_one_step(event, scenario)
            elif event.type == 'stop':
                self.simulation_stopped = True

        if self.emergency_pool:
            raise ValueError(f'simulation for day {day} completed, but emergencies remain: {self.emergency_pool}')

    def planned_schedule_for_remaining_of_day(self, day, t, scenario):
        for r in self.queue.queues[day]:
            qr = [(ev.type, ev.t, ev.j) for ev in self.queue.queues[day][r]]
            qr = [('release', t, job) for job in self.released_but_not_started[r]] + qr
            wr = []
            load = t
            for (evtype,evt,job) in qr:
                if job is None:
                    assert evtype=='completion' and evt == 0 # this is the dummy event at the beginning of the day
                    continue
                if job[0]=='EMER':
                    assert job[1] == day
                    s, _, k = job
                    if k in self.emergency_pool:
                        job_object = self.emergency_pool[k][0]  # prioritize reading from emergency pool,
                                                                # as it may differ from scenario for future days
                    else:
                        job_object = scenario.emergencies[day][k][0]
                else:
                    s,k = job
                    job_object = self.instance.jobs[s][k]
                dmean = job_object.mean

                if evtype == 'release':
                    wr.append((evt, dmean, job))
                elif evtype == 'completion':
                    assert job in self.start
                    d = job_object.EX_conditionned_X_larger_t(t - self.start[job])
                    assert (not self.available_block[r])
                    self.completion[job] = self.start[job] + d
                    load = self.completion[job]

            for rj, dj, job in wr:
                self.start[job] = max(rj, load)
                self.completion[job] = self.start[job] + dj
                load = self.completion[job]
                self.assigned[job] = (day, r)
                self.delay[job] = max(self.start[job] - rj, 0)
                if self.status[job] is None:
                    self.status[job] = 'scheduled'  # do not mark as scheduled if it has been rescheduled !
                self.idle[day,r] += max(self.start[job] - self.load[day,r], 0)
                self.load[day,r] = load

        #put emergencies at the end
        for k in self.emergency_pool:
            job = ('EMER', day, k)
            dj = self.emergency_pool[k][0].mean
            if job in self.assigned and self.assigned[job] is not None:
                continue
            tab = [(self.load[b], b[1]) for b in self.load if b[0]==day]
            r = min(tab)[1]
            self.start[job] = self.load[day,r]
            self.completion[job] = self.start[job] + dj
            self.assigned[job] = (day, r)
            self.delay[job] = 0
            self.status.setdefault(job, None)
            if self.status[job] is None:
                self.status[job] = 'scheduled'  # do not mark as scheduled if it has been rescheduled !
            self.idle[day, r] += max(self.start[job] - self.load[day, r], 0)
            self.load[day, r] = self.completion[job]


    def initialize_day(self, day):
        self.available_block = {}
        self.released_but_not_started = {}
        for r in self.queue.queues[day]:
            self.available_block[r] = False
            self.released_but_not_started[r] = []
            self.queue.insert(Event('completion', 0, r, None, day))  # dummy completion event at the start of the room

    def make_one_step(self, event, scenario):
        done = False
        while not done:
            step = self.online_rule.next_step(event,
                                              self.emergency_pool,
                                              self.queue,
                                              self.released_but_not_started)

            done = self.execute(step, event, scenario)
            if self.debug:
                if step[0] == 'reschedule':
                    print(step, done)

    def execute(self, step, event, scenario):
        """
        executes a `step` returned by the online rule, after having been triggered by `event`, in given `scenario`
        returns True if a job has started or the machine starts idling
        """
        day = event.d
        room = event.r
        t = event.t

        if step[0] == 'start':
            if step[1] == 'EMER':
                k = step[2]
                del self.emergency_pool[k]
                completion = t + scenario.emergencies[day][k][1]
                job = 'EMER', day, k
            else:
                sp, ii = step[1:]
                job = sp, ii
                assert self.released_but_not_started[room][0] == job
                del self.released_but_not_started[room][0]

                st = self.tentative_starts[sp][ii]
                completion = t + scenario.electives[sp][ii]
                self.delay[job] = max(t - st, 0)
                if self.status[job] is None:
                    self.status[job] = 'scheduled'  # do not mark as scheduled if it has been rescheduled !

            self.start[job] = t
            self.completion[job] = completion
            self.queue.insert(Event('completion', completion, room, job, day))
            self.available_block[room] = False
            b = (day, room)
            self.assigned[job] = b
            self.idle[b] += max(t - self.load[b], 0)
            self.load[b] = completion

        elif step[0] == 'reschedule':
            job, new_block = step[1:]
            self.postponed[job] += 1

            # remove job from queue(s)
            if job in [ev.j for ev in self.queue.queues[day][room]]:
                k = [ev.j for ev in self.queue.queues[day][room]].index(job)
                self.queue.remove(day, room, k)
            if job in self.released_but_not_started[room]:
                k = self.released_but_not_started[room].index(job)
                del self.released_but_not_started[room][k]

            if new_block is not None:
                (new_d, new_r, new_t) = new_block
                self.queue.insert(Event('release', new_t, new_r, job, new_d))
                self.status[job] = 'rescheduled'
            else:
                self.status[job] = 'cancelled'

        return step[0] != 'reschedule'

    def select_next(self, room, day, time):
        return self.online_rule.select_next_job(room, day, time, self.emergency_pool,
                                                self.queues, self.waiting_for_jobs[room])

    def get_current_mean_scenatio(self, current_time, scenario):
        """
        This function returns a scenario, given the current time `t` and the present scenario, such that:
        * completed jobs are as in the input scenario
        * non-started jobs have duration equal to their mean processing time
        * ongoing jobs have duration equal to their expectation, conditionned to the event that they are not finished at t
        * The number of emergencies on future days is exactly `instance.emergency_rates`
        """
        current_mean_scenario_electives = {sp: duration[:] for sp, duration in scenario.electives.items()}
        current_mean_scenario_emergencies = {d: emerg[:] for d, emerg in scenario.emergencies.items()
                                             if d <= current_time[0]}
        for d in range(current_time[0] + 1, 5):
            current_mean_scenario_emergencies[d] = []  # we will generate `rates` emergencies for future days

        # make sure that we have exactly `rates` emergencies on each future day
        jobs = [jb for jb in jobs if (jb[0] != 'EMER') or (jb[1] <= current_time[0])]

        for d in range(current_time[0] + 1, 5):
            for j in range(self.instance.emergency_rates):
                jobs.append(('EMER', d, j))
                self.start[('EMER', d, j)] = None

        for job in jobs:
            # retrieve job object and realization
            if len(job) == 2:  # elective job
                sp, j = job
                job_object = self.instance.jobs[sp][j]
                realization = scenario.electives[sp][j]
                if self.assigned[job] is not None:
                    d = self.assigned[job][0]
                else:
                    d = None
            else:  # emergency job
                sp, d, j = job
                if d > current_time[0]:  # future day: consider only `rate` generic emergencies
                    if j >= self.instance.emergency_rates:
                        raise ValueError
                    job_object = type(self.instance).create_job(90, 70 ** 2, 0, 0)
                    realization = None  # do not need a realization, this is in the future anyway
                else:
                    job_object = scenario.emergencies[d][j][0]
                    realization = scenario.emergencies[d][j][1]

            # compute duration dj in the 'current mean scenario'
            if self.start[job] is None:
                dj = job_object.mean
            else:
                Cj = (d, self.completion[job])
                Sj = (d, self.start[job])
                if Sj > current_time:
                    dj = job_object.mean
                elif Cj <= current_time:
                    dj = realization
                else:
                    assert d == current_time[0]
                    dj = job_object.EX_conditionned_X_larger_t(current_time[1] - Sj[1])

            # update the 'current mean scenario'
            if len(job) == 2:  # elective job
                current_mean_scenario_electives[sp][j] = dj
            else:  # emergency job
                if d <= current_time[0]:
                    current_mean_scenario_emergencies[d][j] = (current_mean_scenario_emergencies[d][j][0], dj)
                else:
                    current_mean_scenario_emergencies[d].append((job_object, dj))

        # simulate the mean scenario
        current_mean_scenario = bs.Scenario(current_mean_scenario_electives, current_mean_scenario_emergencies)
        return current_mean_scenario

    def visualize_plotly(self, current_time=(4, None), scenario=None, use_current_mean_scenario=False):
        """
        This function produces a plotly Gantt chart for each day of the week,
        taking into account the current time t:
            * events prior to t are based on observation
            * events after t are `as planned`, using expected values for the visualization
        """
        jobs_data = [[] for d in range(5)]
        if current_time[1] is None:
            current_time = (current_time[0], np.inf)

        if current_time < (4, np.inf):
            assert scenario is not None, 'must provide a scenario when visualizing at non-final time'
            # the current time is not in final state, need to simulate the mean scenario
            if use_current_mean_scenario:
                current_mean_scenario = self.get_current_mean_scenatio(current_time, scenario)
                self.simulate_scenario(current_mean_scenario)
            else:
                self.simulate_scenario(scenario, until=current_time)
        else:
            if not any([(self.start[j] is not None) for j in self.start]):
                assert scenario is not None, 'must either provide a scenario or simulation must have been run before'
                self.simulate_scenario(scenario)

        jobs = list(self.start.keys())
        viz_start = {}
        viz_completion = {}
        viz_assigned = {}
        for job in jobs:
            viz_start[job] = self.start[job]
            viz_completion[job] = self.completion[job]
            viz_assigned[job] = self.assigned[job]

        max_load = {}
        for job in viz_start:
            if viz_assigned[job] is not None and viz_start[job] is not None:  # the job is not in dummy block
                block = viz_assigned[job]
                d = block[0]
                start = viz_start[job]
                finish = viz_completion[job]
                max_load.setdefault(d, 0)
                max_load[d] = max(max_load[d], finish)
                if len(job) == 2:  # elective job
                    sp, j = job
                    tentative = self.tentative_starts[sp][j]
                    delay = self.delay[sp, j]
                else:
                    tentative = 0.
                    delay = 0.

                job_dict = dict(Task="Job" + str(job),
                                    IntStart=int(start),
                                    IntCompletion=int(finish),
                                    Start=f'{start:.3f}',
                                    Completion=f'{finish:.3f}',
                                    Block=str(block),
                                    Tentative_Start=f'{tentative:.3f}',
                                    Delay=f'{delay:.3f}',
                                    Specialty=job[0],
                                    Completed=((d, finish) <= current_time),
                                    Group=job[0])
                jobs_data[d].append(job_dict)
        figure_list = []
        days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        for d in range(5):
            df = pd.DataFrame(jobs_data[d])
            df['Duration'] = df['IntCompletion'] - df['IntStart']
            fig = px.timeline(df, x_start='IntStart', x_end='IntCompletion', y='Block', color='Group',
                              color_discrete_map={
                                  "CARD": px.colors.qualitative.Plotly[0],
                                  "GASTRO": px.colors.qualitative.Plotly[2],
                                  "GYN": px.colors.qualitative.Plotly[6],
                                  "ORTH": px.colors.qualitative.Plotly[3],
                                  "URO": px.colors.qualitative.Plotly[4],
                                  "EMER": px.colors.qualitative.Plotly[1],
                                  "MED": px.colors.qualitative.Plotly[9],
                              },
                              color_discrete_sequence=px.colors.qualitative.G10,
                              hover_data={'Start':True,
                                          'Completion':True,
                                          'Task': True,
                                          'Block': False,
                                          'Specialty': False,
                                          'Duration':True,
                                          'Tentative_Start':True,
                                          'Group': False,
                                          'IntStart': False,
                                          'IntCompletion': False,
                                          'Delay': True
                                          },
                              title=days[d])
            fig.update_yaxes(autorange='reversed')
            fig.layout.xaxis.type = 'linear'
            # fig.data[0].x = df.delta.tolist()
            # Hack: Set x values for every group of jobs in the figure manually to the integer duration
            #       necessary since px.timeline only works properly for dates
            for i in range(len(fig.data)):
                fig.data[i].x = df.Duration[df.Group == fig.data[i].offsetgroup].tolist()

            fig.update_xaxes(range=[0, max(600,max_load[d]+20)])
            if current_time[0] <= d:
                if current_time[0] == d:
                    x0 = current_time[1]
                    if np.isinf(x0):
                        x0 = max_load[d]
                else:
                    x0 = 0
                x1 = max(600,max_load[d]+20)
                fig.add_vrect(x0=x0,x1=x1,fillcolor='white',opacity=0.5,line_width=0)
                if current_time[0] == d:
                    fig.add_vline(x0)

            fig.update_layout(margin=dict(l=0, r=0, t=35, b=20))
            figure_list.append(fig)

        return figure_list

class OnlineRule:
    """
    parent class for online rules
    """

    def __init__(self, alpha=0.8, delta=10, delta2=90):
        self.policy = None  # will be set in policy's constructor
        self.alpha = alpha
        self.delta = delta
        self.delta2 = delta2

    def next_step(self, event, emergency_pool, queue, released_not_started):
        """
        Choose next step with state given by input, when `event` has just been triggered.
        The function can either return:
          * ('start', 'EMER', k)     -> start the kth job from the emergency pool
          * ('start', specialty, k)  -> start the kth job of specialty. This job must be 1st in `released_not_started`
          * ('reschedule', (specialty, k), new_block)
            -> reschedules the kth job of specialty. This job must be in queue.queues[d][r]
               The job is cancelled (put in dummy block) if `new_block` is `None`
               or scheduled in block `(dd, rr)` at `tt` if `new_block = (dd,rr,tt)`
          * ('idle',) only if emergency_pool and released_not_started is empty

        Behaviour of default function:
            * if there is a released job ready to start:
                _ check if expected load of room exceeds 480 + delta2
                    -> in that case, reschedule the last waiting job
                _ or start the elective job if current_time <= 480 + delta1 (otw, reschedule it)
            * otherwise, try to start an emergency:
                _ the longest that "fit" in the idle time
                _ or the shortest expected emergency with alpha * mean-duration less than idle time
                _ if no emergency was added, idle
        """
        room = event.r
        if released_not_started[room]:
            s, k = released_not_started[room][0]
            mean_duration = self.policy.instance.jobs[s][k].mean
            current_job = (s,k)
            last_r_job = (s,k)

            if self.delta2 <= 300:
                #builds current expected load, taking emergencies into account
                current_exp_loads = {}
                for r in released_not_started:
                    current_exp_load = event.t
                    waiting_list_r = [(current_exp_load, job) for job in released_not_started[r]]
                    for ev in queue.queues[event.d][r]:
                        if ev.type=='release':
                            waiting_list_r.append((ev.t,ev.j))
                        elif ev.type=='completion':
                            if ev.j[0] == 'EMER':
                                dj = self.policy.instance.emergency_mean_std[0]  # TODO use posterior instead (need scenario in input)
                            else:
                                dj = self.policy.instance.jobs[ev.j[0]][ev.j[1]].mean
                            current_exp_load = max(current_exp_load, self.policy.start[ev.j] + dj)
                    for rj,job in waiting_list_r:
                        dj = self.policy.instance.jobs[job[0]][job[1]].mean
                        if r==room and len(job)==2:
                            last_r_job = (job[0], job[1])
                        current_exp_load = max(current_exp_load,rj) + dj
                    current_exp_loads[r] = current_exp_load
                for kem, em in emergency_pool.items():
                    rmin = min(current_exp_loads, key=current_exp_loads.get)
                    current_exp_loads[rmin] += em[0].mean


                can_start = (current_exp_loads[room] <= 480 + self.delta2) and (event.t <= 480 + self.delta)
            else:
                can_start = (event.t <= 480 + self.delta)

            if can_start:
                s, k = current_job
                return 'start', s, k
            else:
                s, k = last_r_job
                # try to insert longest job with first-fit
                for d, r in self.policy.instance.blocks[s]:
                    self.policy.expload.setdefault((d,r),0.)
                    if d > event.d and self.policy.expload[d, r] + mean_duration <= 480 + self.delta:
                        new_t = self.policy.expload[d, r]
                        self.policy.expload[d, r] += mean_duration
                        if self.policy.debug:
                            print('reschedule', (s, k), (d, r, new_t), 'on', str(event))
                        return 'reschedule', (s, k), (d, r, new_t)
                if self.policy.debug:
                    print('cancel', (s, k), 'on', str(event))
                return 'reschedule', (s, k), None
        else:
            if not emergency_pool:
                return 'idle',
            else:
                means = {k: em.mean for k, (em, _) in emergency_pool.items()}
                longest_expected_emer = max(means, key=means.get)
                if not (
                        queue.queues[event.d][
                            room]):  # no elective case scheduled in this room -> start longest emergency
                    return 'start', 'EMER', longest_expected_emer
                else:
                    tstart = queue.queues[event.d][room][0].t
                    idle_time = tstart - event.t
                    means_fit = {k: em.mean for k, (em, _) in emergency_pool.items() if em.mean <= idle_time}
                    if means_fit:
                        longest_fitting_emer = max(means_fit, key=means_fit.get)
                        return 'start', 'EMER', longest_fitting_emer
                    means_alphafit = {k: em.mean for k, (em, _) in emergency_pool.items() if
                                      self.alpha * em.mean <= idle_time}
                    if means_alphafit:
                        shortest_alphafit = min(means_alphafit, key=means_alphafit.get)
                        return 'start', 'EMER', shortest_alphafit
                    else:
                        return 'idle',
