# script to read the pandas dataframes with all results
# created with `run_experiments.py`, and to visualize boxplots showing the distribution of results

import pandas as pd
import plotly.express as px

import plotly.io as pio

pio.renderers.default = 'browser'

df = pd.DataFrame()
nseed = 0

for i in range(1, 31):
    dfi = pd.read_json(f'results/results{i}.json')
    if len(dfi) == 4032:
        nseed += 1
        print(f'concatenating {i}th table')
        df = pd.concat([df, dfi], ignore_index=True)

df['algo'] = df['method'] + df['capacity'].astype(str) + df['maxemer'].astype(str)
df['algo'] = df['algo'].replace({'deterministicnannan': 'DET',
                                 'first-fit480.0nan': 'FF-480',
                                 'first-fit500.0nan': 'FF-500',
                                 'first-fit520.0nan': 'FF-520',
                                 '2stage-stochasticnan0.0': '2SS-0',
                                 '2stage-stochasticnan5.0': '2SS-5',
                                 '2stage-stochasticnan10.0': '2SS-10',
                                 })

#######################################################
# compare costs for different optimization method

n = 200
costs = '1-.5-.25'  # '1-.5-.25' | '1-0-0'
unit = 'week'
alpha = 0.9
Delta = 120

data = df[(df['n'] == n) & (df['costs'] == costs) & (df['flowtime_unit'] == unit) & (df['alpha'] == alpha) & (
        df['Delta'] == Delta) & (df['algo'].isin(['DET', 'FF-480', 'FF-520', '2SS-0', '2SS-10']))]

data['secondstage_costs'] = data['idling_cost'] + data['overtime_cost'] + data['waiting_cost']

# total cost per algorithm and emergency rate
fig = px.box(data, x="algo", y="total_costs", color='rates',
             width=800, height=400,
             title="Total cost per algorithm and emergency rate",
             )
fig.show()

# second-stage costs per algorithm
fig = px.box(data, x="algo", y="secondstage_costs", color='rates', width=800, height=400,
             title="Box plot of second-stage costs",
             )
fig.show()

#######################################################    
# see the effect of alpha and Delta
n = 200
costs = '1-0-0'  # '1-.5-.25' | '1-0-0'
unit = 'day'
algo = '2SS-10'
rate = 2

data = df[(df['n'] == n) & (df['costs'] == costs) & (df['flowtime_unit'] == unit) & (df['algo'] == algo) & (
        df['rates'] == rate)]

# With Delta=120, we get much less overtime, but this does not compensate the increase of cost due to cancellations (
# but this is due to our choice of the cost parameters)
fig = px.box(data, x="Delta", y="total_costs")
fig.show()

fig = px.box(data, x="Delta", y="overtime_cost")
fig.show()

###############################################################################
# total cost per algorithm and scenario cost
n = 200
unit = 'day'
alpha = 0.9
Delta = 120
rate =2

data = df[(df['n'] == n) & (df['flowtime_unit'] == unit) & (df['alpha'] == alpha) & (
        df['Delta'] == Delta) & (df['algo'].isin(['DET', 'FF-480', 'FF-520', '2SS-0', '2SS-10'])) & (
        df['rates'] == rate)]

fig = px.box(data, x="algo", y="total_costs", color='costs',
             width=800, height=400,
             title="Big instances (n=200), time measured in day ",
             )
fig.show()

###############################################################################
# see how do all the cost parameters c^o c^w c^i compare to each other
n = 100
unit = 'day'
alpha = 0.8
Delta = 120
rate = 1

data = df[(df['n'] == n) & (df['flowtime_unit'] == unit) & (df['alpha'] == alpha) & (df['Delta'] == Delta) & (
        df['rates'] == rate)]

fig = px.box(data, x="algo", y="total_costs", color='costs',
             title="Box plot of total cost for 100 patients and for different cost parameters (Emergency rate 1)(delta=120, alpha=0.8, flowtime =day)",
             )
fig.show()