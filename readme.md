# MOPTA2022-TU_Berlin

MOPTA2022-TU_Berlin is the code of a submission for the MOPTA 2022 competition
for the team TU Berlin

* Members:
    * Mohammed Majthoub Almoghrabi
    * Przemyslaw Bartman
* Advisor
    * Guillaume Sagnol

## Installation

Download the code by cloning this gitlab repository.
Then, we recommand to create a virtual environment with all required packages.

```bash
git clone git@gitlab.com:majthoub/mopta2022-tu_berlin.git
cd mopta2022-tu_berlin
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt 
```

If the above fails, make sure that your python installation is up-to-date:
```bash
apt-get update -qy
apt-get install -y python3-dev python3-pip
pip3 install virtualenv
```

## Usage

The user interface can be run in a browser by typping. Settings should be either self-explanatory,
or detailed in the submitted report.
```bash
python3 dashapp.py
```

## Code
The code is separated in different files:
* `block_scheduling.py` defines following classes:
    * `BlockSchedulingInstance` stores an instance of the elective surgery planning problem (ESP)
    * `Criterion` is used to store all the parameters for evaluating a scheduling policy
    * `Scenario` is used to store a particular realization of the uncertainty
    
* `assignments.py` contains a parent class `Assignment` which is used to store the output
  of the offline phase (assignment and sequencing of patient in blocks, as well as
  tentative starting times). This class has three children that implement an optimization
  method to compute an assignment upon initialization:
    * `DeterministicAssignment` solves the assignment problem for a deterministic(ized) instance
    * `TwoStageStochasticAssignment` implements the two-stage stochastic programming approach
       detailed in the submitted report
    * `FirstFitAssignment` implements the first-fit heuristic
    
* `policies.py` is used to simulate a policy with a discrete event simulation.
  * The discrete event simulator relies on the class `Event` and `EventQueue`
  * A policy is an instance of `BlockSchedulingPolicy`, and consists of:
    * An `Assignment` which is the output of the offline phase (together with tentative starting times)
    * An `OnlineRule` which defines how to handle the uncertainty, and
      must implement a method `next_step()` that selects the action to execute when
      an `Event` is triggered. The default behaviour of `next_step()` is detiailed
      in the submitted report.
    
* `stoch_job.py` contains utilities to work with jobs of stochastic durations, in particular
  with a lognormal distribution.

## License
[MIT](LICENSE.txt)
