# This script was used to produce a pandas dataframe with results for different instances, algos and parameters

import sys
sys.path = sorted(sys.path, key=lambda x: ('venv' in x), reverse=True)
import policies as pol
import block_scheduling as bs
import assignments as ass
import pandas as pd
import itertools

seed = int(sys.argv[1])
print(f'starting experiments for seed {seed}')


tab_n = [70,100,140,200]
tab_second_stage_costs = ['1-.5-.25', '1-.5-0', '1-0-0']
tab_flowtime_unit = ['day', 'week']
tab_emergency_rate = [0,1,2,3]
tab_method = ['deterministic', '2stage-stochastic','first-fit']
tab_capacity = [480,500,520]
tab_maxemer = [0,5,10]
tab_delta2 = [1000, 120]
tab_alpha = [0.7,0.8,0.9]


method_with_params = []
for m in tab_method:
    if m=='deterministic':
        method_with_params.append((m,None,None))
    elif m=='2stage-stochastic':
        for maxemer in tab_maxemer:
            method_with_params.append((m,None,maxemer))
    else:
        for capa in tab_capacity:
            method_with_params.append((m,capa, None))


df = pd.DataFrame()

for n, rate in itertools.product(tab_n,tab_emergency_rate):
    print(n,rate)
    instance = bs.BlockSchedulingInstance.from_seed(n, rate, seed)
    instance.build_train_test_sets(200, 0, do_seed=True, random_seed=1)

    for second_stage_costs, flowtime_unit in itertools.product(tab_second_stage_costs, tab_flowtime_unit):
        co, cw, ci = [float(c) for c in second_stage_costs.split('-')]
        if flowtime_unit == 'week':
            w0 = 19
        else:
            w0 = 0.43
        crit = bs.Criterion(instance,
                            waiting_cost=cw,
                            idling_cost=ci,
                            w0=w0,
                            flowtime_unit=flowtime_unit)

        q = 0.7
        for method, capacity, maxemer in method_with_params:
            if method == 'deterministic':
                asg = ass.DeterministicAssignment(instance, crit,
                                                  mip_quantile=q,
                                                  strict_start=False)
            elif method == '2stage-stochastic':
                asg = ass.TwoStageStochasticAssignment(instance, crit,
                                            max_considered_emergencies=maxemer,
                                            number_scenarios_for_setting_tentatives=250,
                                            timelimit=20)
            elif method == 'first-fit':
                print('FIRSTFIT')
                asg = ass.FirstFitAssignment(instance, crit, quantile=q, capacity=capacity)

            for alpha,Delta in itertools.product(tab_alpha, tab_delta2):
                bsp = pol.BlockSchedulingPolicy(instance, asg.full_assignment, asg.tentatives,
                                                pol.OnlineRule(alpha=alpha, delta=1000, delta2=Delta))

                print(f'start simulation for n={n}, rate={rate}, costs={second_stage_costs}, unit={flowtime_unit}, method={method}, capacity={capacity}, maxemer={maxemer}, alpha={alpha}, Delta={Delta}...')
                _, _, mean_total_costs, mean_costs = bsp.evaluate(crit, train=True)
                print('done.')

                row = {'seed': seed,
                       'n': n,
                       'costs': second_stage_costs,
                       'flowtime_unit': flowtime_unit,
                       'method': method,
                       'capacity': capacity,
                       'maxemer': maxemer,
                       'alpha': alpha,
                       'Delta': Delta,
                       'total_costs': mean_total_costs,
                       'rate': rate}

                for k,v in mean_costs.items():
                    row[k] = v

                df = pd.concat([df, pd.DataFrame.from_records([row])], ignore_index=True)
                df.to_json(f'results{seed}.json', indent=4)