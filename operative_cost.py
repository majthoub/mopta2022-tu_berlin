# This script was used in a preprocessing step to compute the
# coefficients of the piecewise linear approximation of the
# second-stage costs stored in operative_costs

import gurobipy as gp
from gurobipy import GRB
import numpy as np

import stoch_job as sj
import scipy.stats as sst

import block_scheduling as bs

import json

xx = []
yy = []
NN = 300

for it in range(NN):

    print(it)

    # Create operative cost model
    model = gp.Model('lp')

    specialty = 'ORTH'
    if specialty == 'CARD':
        mean, std = (99, 53)
    elif specialty == 'URO':
        mean, std = (72, 38)
    elif specialty == 'GASTRO':
        mean, std = (132, 76)
    elif specialty == 'GYN':
        mean, std = (78, 52)
    elif specialty == 'MED':
        mean, std = (75, 72)
    elif specialty == 'ORTH':
        mean, std = (142, 58)

    # number of jobs
    lb = int(300 / mean)
    ub = int(np.ceil(550 / mean))
    n = np.random.randint(lb, ub + 1)

    K = 3000  # number of scenarios

    cw = 0.5   # waiting cost
    ci = 0.25   # idle cost
    co = 1  # cost

    # pmean = np.random.uniform(100, 200, n)
    # p = (np.random.uniform(-50, 50, (K, n)) + pmean).T

    jobs = []
    for i in range(n):
        job = bs.BlockSchedulingInstance.get_posterior(mean, std, 0.25, 0., 0.)
        jobs.append(job)

    # sort by smallest variance first
    jobs = sorted(jobs, key=lambda x: x.var)

    p = np.array([job.rvs(size=K) for job in jobs])

    T = 480  # overtime

    # Create variables
    s = model.addVars(n, K, name='s')
    t = model.addVars(n, name='t')
    ovt = model.addVars(K, name='ovt')

    # Set objective
    load = []
    obj = 0
    for k in range(K):
        load.append(s[n - 1, k] + p[n - 1, k])
        scenario_cost = 0
        for i in range(n):
            scenario_cost += cw * (s[i, k] - t[i])

        scenario_cost += ci * (load[k] - sum(p.T[k]))
        scenario_cost += co * ovt[k]

        obj += 1. / K * scenario_cost

    model.setObjective(obj, GRB.MINIMIZE)

    # Add Constraints
    for k in range(K):
        model.addConstr(ovt[k] >= load[k] - T, name='overtime ' + str(k))

        for i in range(n):
            model.addConstr(s[i, k] - t[i] >= 0, name='t (' + str(k) + ',' + str(i) + ')')
            if i >= 1:
                model.addConstr(s[i, k] - s[i - 1, k] >= p[i - 1, k], name='p (' + str(k) + ',' + str(i) + ')')

    # Optimize model
    model.optimize()

    xx.append(sum([job.mean for job in jobs]))
    yy.append(model.getObjective().getValue())

# Convex piecewise-linear fitting
kmax = 5
ninit = 10

xx1 = np.r_[xx, np.arange(0, 100, ninit)]
yy1 = np.r_[yy, [0] * len(np.arange(0, 100, ninit))]

I = np.argsort(xx1)

aaa = {}
bbb = {}
for nparts in [3, 5, 7]:
    if nparts == 3:
        breakpoints = [360, 510]
    elif nparts == 5:
        breakpoints = [100, 400, 470, 540]
    elif nparts == 7:
        breakpoints = [100, 250, 380, 420, 480, 550]

    breakpoints = [0] + breakpoints + [10000]
    parts = []
    for l, u in zip(breakpoints[:-1], breakpoints[1:]):
        parts.append(np.where((xx1 >= l) & (xx1 < u))[0])

    a_list = []
    b_list = []
    for part in parts:
        x = np.array(xx1)[part]
        y = np.array(yy1)[part]
        A = np.vstack([x, np.ones(len(x))]).T
        a, b = np.linalg.lstsq(A, y, rcond=None)[0]
        a_list.append(a)
        b_list.append(b)

    figure(nparts)
    scatter(xx1, yy1)
    xxx = np.linspace(0, 1300, 301)
    plot(xxx, [max([a * x + b for a, b in zip(a_list, b_list)]) for x in xxx], c='r', linewidth=3, label=str(nparts))
    legend()
    aaa[nparts] = a_list
    bbb[nparts] = b_list

# save a, b values to json file
file_name = './operative_costs/cw{0}_ci{1}_co{2}.json'.format(cw, ci, co)

try:
    with open(file_name, 'r') as fp:
        old_results = json.load(fp)
except FileNotFoundError:
    old_results = {}

result = {str(specialty): {}}
result[str(specialty)]['a'] = aaa
result[str(specialty)]['b'] = bbb
old_results.update(result)
new_results = old_results
with open(file_name, 'w') as fp:
    json.dump(new_results, fp, sort_keys=True, indent=4)

"""
scatter(xx1, yy1)
xxx = np.linspace(0, 800, 101)

for kk in range(kmax):
    a_list = []
    b_list = []
    for part in parts:
        x = np.array(xx1)[part]
        y = np.array(yy1)[part]
        A = np.vstack([x, np.ones(len(x))]).T
        a, b = np.linalg.lstsq(A, y, rcond=None)[0]
        a_list.append(a)
        b_list.append(b)

    plot(xxx, [max([a * x + b for a, b in zip(a_list, b_list)]) for x in xxx], label=str(kk))

    new_parts = []
    part2 = parts[0]
    for kn in range(npart - 1):
        Upart = np.r_[part2, parts[kn + 1]]
        x = np.array(xx1)[Upart]
        nu = len(Upart)
        tab = []
        for j in range(nmin, nu - (nmin - 1)):
            part1 = Upart[:j]
            part2 = Upart[j:]
            x1 = np.array(xx1)[part1]
            y1 = np.array(yy1)[part1]
            x2 = np.array(xx1)[part2]
            y2 = np.array(yy1)[part2]
            A1 = np.vstack([x1, np.ones(len(x1))]).T
            A2 = np.vstack([x2, np.ones(len(x2))]).T
            (a1, b1), r1, _, _ = np.linalg.lstsq(A1, y1, rcond=None)
            (a2, b2), r2, _, _ = np.linalg.lstsq(A2, y2, rcond=None)
            tab.append((r1 + r2, a1, b1, a2, b2, j))
        j = min(tab)[-1]
        part1 = Upart[:j]
        part2 = Upart[j:]
        new_parts.append(part1)

    new_parts.append(part2)
    parts = [prt[:] for prt in new_parts]

legend()

"""

